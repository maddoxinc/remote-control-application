﻿using ProgettoGUI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProgeetoGUI
{
    public partial class Form1 : Form
    {

        public Worker workerObject;
        public ArrayList active_connections;
        public int active_index = -1;
        Form2 FullscreenForm;
        static bool IsHook = false;
        public delegate void standbyConnection();
        private readonly object sync = new object();
        Thread thread_control;
        String shortcut = null;
        public String minimize = "M, Control";
        public String standby = "S, Control";
        public String ClientToServ = "C, Alt";
        public String servToClient = "S, Alt";
        Transfer form_transfer;
        public bool isTransferring;


        public Form1()
        {
            InitializeComponent();
            pictureBox1.Visible = false;
            pictureBox2.Visible = true;
            label5.Text = "Disconnesso";
            FullscreenForm = new Form2();
            form_transfer = new Transfer();
            form_transfer.TopMost = true;
            FullscreenForm.SetPointer(this);
            form_transfer.SetPointer(this);
            active_connections = new ArrayList();
            active_index = -1;

            label1.Text = standby;
            label2.Text = minimize;
            label9.Text = ClientToServ;
            label13.Text = servToClient;

            serverList.View = View.Details;
            serverList.GridLines = true;
            serverList.FullRowSelect = true;

            serverList.Columns.Add("Server name", 180);
            serverList.Columns.Add("Server address", 180);
            serverList.Columns.Add("Server port", 220);
            serverList.Columns.Add("hotkey", 148);
            isTransferring = false;
        }

        public void showFormTransfer()
        {
            if (!form_transfer.IsDisposed)
                form_transfer.Show();
            else
            {
                form_transfer = new Transfer();
                form_transfer.TopMost = true;
                form_transfer.Show();
            }
        }

        public void hideFormTransfer()
        {
            if (!form_transfer.IsDisposed)
                form_transfer.Hide();
        }

        public void setIsTransferring(bool flag, AutoResetEvent ev)
        {
            isTransferring = flag;
            if (ev != null)
            {
                ev.Set();
            }
        }

        private void button4_Click(object sender, EventArgs e)// standby in Info
        {
             standby_func();         
        }


        private void button3_Click(object sender, EventArgs e) // tasto Connetti in Info
        {
           
            ManagerConnect new_connection;
            XmlSerializer xs;

            try
            {
                if (active_index != -1)
                {
                    ((ManagerConnect)active_connections[active_index]).deactivateDelegate();
                    String notarget = "notarget";
                    Stream stream2 = new MemoryStream();
                    xs = new XmlSerializer(typeof(String));
                    xs.Serialize(stream2, notarget);
                    send(stream2, ((ManagerConnect)active_connections[active_index]).getSock_control());

                    label5.Text = "Disconnesso";
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = true;
                    FullscreenForm.Hide();
                    active_index = -1;
                }


                if (!String.IsNullOrEmpty(textBox4.Text) && !String.IsNullOrEmpty(ipBox.Text) && !String.IsNullOrEmpty(portBox.Text))
                {
                    int res;
                    bool success = int.TryParse(portBox.Text, out res);
                    if (success && res > 1024)
                    {
                        IPAddress  ipa;
                        success = IPAddress.TryParse(ipBox.Text, out ipa);
                        if (success)
                        {
                            
                            new_connection = new ManagerConnect(textBox4.Text, ipBox.Text, int.Parse(portBox.Text), FullscreenForm, this, shortcut);
                            if (new_connection.sock_control == null)
                                throw new SocketException();
                            //with textbox
                            active_index = active_connections.Add(new_connection);

                            FullscreenForm.SetConnection(new_connection);
 
                        }
                        else
                        {
                            MessageBox.Show("Inserire indirizzo IP valido per la connessione", "Avviso",
                                     MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Inserire numero di porta valido( > 1024 ) per la connessione", "Avviso",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else
                {
                    MessageBox.Show("Inserire nome,IP e porta validi per la connessione", "Avviso",
                           MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Eccezione gestita correttamente");
            }

        }

        public void connetti()
        {

            string[] arr = new string[4];
            ListViewItem item;

            //add items to ListView
            try
            {
                arr[0] = ((ManagerConnect)active_connections[active_index]).getName();
                arr[1] = ((ManagerConnect)active_connections[active_index]).getAddress();
                int port = ((ManagerConnect)active_connections[active_index]).getPort();
                arr[2] = port.ToString() + ", " + (port + 1).ToString() + ", " + (port + 2).ToString() + ", " + (port + 3).ToString();
                arr[3] = shortcut;
                item = new ListViewItem(arr);
                serverList.Items.Add(item);

                label5.Text = "Connesso";
                pictureBox1.Visible = true;
                pictureBox2.Visible = false;

                thread_control = new Thread(((ManagerConnect)active_connections[active_index]).connetti);
                ((ManagerConnect)active_connections[active_index]).setControlThread(thread_control);

                thread_control.Start();

                textBox4.Text = "";
                textBox5.Text = "";
                ipBox.Text = "";
                portBox.Text = "";
               
            }
            catch (Exception )
            {
                this.Close();
            }

            return;
        }

        public void StopHook()
        {
            if (IsHook)
            {
                InputHook.Stop();
                IsHook = false;
            }
        }

        public void StartHook()
        {
            if (!IsHook)
            {
                InputHook.Start();
                IsHook = true;
            }
        }


        private void button5_MouseClick(object sender, MouseEventArgs e)//Tasto riconnetti
        {           
            target(-1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (serverList.SelectedIndices.Count == 1)
                {
                    if (isTransferring == true)
                    {
                        DialogResult dialogResult = MessageBox.Show("Trasferimento file in corso, forzare la chiusura?", "Attenzione!", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.No)
                        {
                            return;
                        }

                        setIsTransferring(false, null);
                    }


                    int selected_index = serverList.SelectedIndices[0];

                    if (selected_index == active_index)
                    {
                       ((ManagerConnect)active_connections[active_index]).deactivateDelegate();
                    }


                    ManagerConnect selected_connection = (ManagerConnect)active_connections[selected_index];

                    String target = "disconnect";
                    Stream stream = new MemoryStream();
                    XmlSerializer xs = new XmlSerializer(typeof(String));
                    xs.Serialize(stream, target);
                    send(stream, selected_connection.getSock_control());

                    FullscreenForm.ResetConnection();

                    closeConnectionWithServer(selected_connection);
                    Console.WriteLine("Connessione chiusa col server " + selected_index.ToString());
                    
                    active_connections.RemoveAt(selected_index);
                    serverList.Items[selected_index].Remove();

                    
                    if (selected_index == active_index)
                    {
                        active_index = -1;
                        label5.Text = "Disconnesso";
                        pictureBox1.Visible = false;
                        pictureBox2.Visible = true;
                        FullscreenForm.WindowState = FormWindowState.Minimized;
                        FullscreenForm.Hide();
                        form_transfer.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("Nessun server selezionato!!!");
                }
            }
            catch (Exception) 
            {
                this.Close();
            }
        }

        public void closeConnectionWithServer(ManagerConnect selected_connection)
        {
            try
            {
                if (selected_connection.sock_control != null)
                {
                    selected_connection.sock_control.Shutdown(SocketShutdown.Both);
                    selected_connection.sock_control.Close();
                    selected_connection.sock_control = null;
                }

                if (selected_connection.sock_recClipboard != null)
                {
                    selected_connection.sock_recClipboard.Shutdown(SocketShutdown.Both);
                    selected_connection.sock_recClipboard.Close();
                    selected_connection.sock_recClipboard = null;
                }
                if (selected_connection.sock_clipboard != null)
                {
                    selected_connection.sock_clipboard.Shutdown(SocketShutdown.Both);
                    selected_connection.sock_clipboard.Close();
                    selected_connection.sock_clipboard = null;
                }
                if (selected_connection.sock_input != null)
                {
                    selected_connection.sock_input.Shutdown(SocketShutdown.Both);
                    selected_connection.sock_input.Close();
                    selected_connection.sock_input = null;
                }

                if (selected_connection.worker_clipboard != null)
                {
                    selected_connection.worker_clipboard.block_event.Set();
                    selected_connection.worker_clipboard.transfer_event.Set();
                    selected_connection.thread_clipboard.Join();
                    selected_connection.thread_recClipboard.Join();
                    setIsTransferring(false, null);
                    thread_control.Abort();
                    thread_control.Join();
                }
            }catch(Exception )
            {
                this.Close();
            }
        }

        public void target(int index)
        {
            
            XmlSerializer xs;
            int selected_index;

            try
            {
                if (index == -1)
                {
                    if (serverList.SelectedIndices.Count == 0)
                    {
                        MessageBox.Show("Nessun server selezionato!!!");
                        return;
                    }
                    selected_index = serverList.SelectedIndices[0];
                }
                else //shortcut connessione premuto
                {
                    selected_index = index;
                }

                if (selected_index == active_index)
                {
                    MessageBox.Show("Il server selezionato è già quello attivo!");
                    return;
                }

                if (active_index != -1)
                {
                    ((ManagerConnect)active_connections[active_index]).deactivateDelegate();
                    String notarget = "notarget";
                    Stream stream2 = new MemoryStream();
                    xs = new XmlSerializer(typeof(String));
                    xs.Serialize(stream2, notarget);
                    send(stream2, ((ManagerConnect)active_connections[active_index]).getSock_control());
                }

                ManagerConnect selected_connection = (ManagerConnect)active_connections[selected_index];
                selected_connection.activateDelegate();
                String target = "target";
                Stream stream = new MemoryStream();
                xs = new XmlSerializer(typeof(String));
                xs.Serialize(stream, target);
                send(stream, selected_connection.getSock_control());
                active_index = selected_index;
                FullscreenForm.SetConnection(selected_connection);
                FullscreenForm.Show();
                FullscreenForm.WindowState = FormWindowState.Maximized;

                label5.Text = "Connesso";
                pictureBox1.Visible = true;
                pictureBox2.Visible = false;
            }
            catch (Exception )
            {
                this.Close();
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            int index = 0;

            if (minimize.CompareTo(e.KeyData.ToString()) == 0)
            {

                FullscreenForm.WindowState = FormWindowState.Maximized;
            }

            foreach (ManagerConnect c in active_connections)
            {
                if (c.shortcut != null)
                {
                    if (e.KeyData.ToString().CompareTo(c.shortcut) == 0)
                    {
                        target(index);
                        break;
                    }
                }
                index++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox8.Text.CompareTo("") != 0)
            {
                if (textBox8.Text.Length > 1)
                {
                    if(textBox8.Text.CompareTo(textBox9.Text)==0 || textBox8.Text.CompareTo(textBox2.Text)==0 || textBox8.Text.CompareTo(textBox1.Text)==0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else if (textBox8.Text.CompareTo(label2.Text) == 0 || textBox8.Text.CompareTo(label9.Text) == 0 || textBox8.Text.CompareTo(label13.Text) == 0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else
                    {
                        FullscreenForm.sStandby = standby;
                        label1.Text = textBox8.Text;
                    }     
                }
                else 
                {
                    MessageBox.Show("shortcut Standby non valida");
                }

                textBox8.Text = "";
            }

            if (textBox9.Text.CompareTo("") != 0)
            {
                minimize = textBox9.Text;
                if (minimize.Length > 1)
                {
                    if (textBox9.Text.CompareTo(textBox8.Text) == 0 || textBox9.Text.CompareTo(textBox1.Text) == 0 || textBox9.Text.CompareTo(textBox2.Text) == 0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else if (textBox9.Text.CompareTo(label1.Text) == 0 || textBox9.Text.CompareTo(label9.Text) == 0 || textBox9.Text.CompareTo(label13.Text) == 0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else
                    {
                        FullscreenForm.sMinimize = minimize;
                        label2.Text = textBox9.Text;
                    }    
                }
                else
                {
                    
                    MessageBox.Show("shortcut minimizza non valida");
                }

                textBox9.Text = "";
            }

            if (textBox1.Text.CompareTo("") != 0)
            {
                if (textBox1.Text.Length > 1)
                {
                    if (textBox1.Text.CompareTo(textBox8.Text) == 0 || textBox1.Text.CompareTo(textBox9.Text) == 0 || textBox1.Text.CompareTo(textBox2.Text) == 0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else if (textBox1.Text.CompareTo(label1.Text) == 0 || textBox1.Text.CompareTo(label2.Text) == 0 || textBox1.Text.CompareTo(label13.Text) == 0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else
                    {
                        FullscreenForm.sClientToServ = ClientToServ;
                        label9.Text = textBox1.Text;
                    }    
                }
                else 
                {
                    MessageBox.Show("shortcut clipboard C->S non valida");
                }

                textBox1.Text = "";
            }

            if (textBox2.Text.CompareTo("") != 0)
            {
                if (textBox2.Text.Length > 1)
                {
                    if (textBox2.Text.CompareTo(textBox8.Text) == 0 || textBox2.Text.CompareTo(textBox9.Text) == 0 || textBox2.Text.CompareTo(textBox1.Text) == 0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else if (textBox2.Text.CompareTo(label1.Text) == 0 || textBox2.Text.CompareTo(label2.Text) == 0 || textBox2.Text.CompareTo(label9.Text) == 0)
                    {
                        MessageBox.Show("shortcut già esistente!");
                    }
                    else
                    {
                        FullscreenForm.sServToClient = servToClient;
                        label13.Text = textBox2.Text;
                    }    
                }
                else 
                {
                    MessageBox.Show("shortcut clipboard S->C non valida");
                }

                textBox2.Text = "";
            }
            
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            shortcut = e.KeyData.ToString();
            if (shortcut.Contains(","))
            {
                textBox5.Text = shortcut;
            }
            else
            {
                textBox5.Text = "";
                shortcut = "";    
            }
        }

        private void textBox8_KeyDown(object sender, KeyEventArgs e)
        {
            standby = e.KeyData.ToString();
            if (standby.Contains(","))
            {
                textBox8.Text = standby;
            }
            else
            {
                textBox8.Text = "";
                
            }
        }

        private void textBox9_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                String tmp = e.KeyData.ToString();
                if (tmp.Contains(","))
                {
                    textBox9.Text = tmp;
                }
                else
                {
                    textBox9.Text = "";
                }
            }
            catch(Exception)
            {
                this.Close();
            }
        }

        public void standby_func()
        {
            try
            {    
                if (active_index == -1)
                {
                    MessageBox.Show("non ci sono server attivi");
                    return;
                }

                ((ManagerConnect)active_connections[active_index]).deactivateDelegate();

                FullscreenForm.WindowState = FormWindowState.Minimized;


                String notarget = "notarget";
                Stream stream2 = new MemoryStream();
                XmlSerializer xs;
                xs = new XmlSerializer(typeof(String));
                xs.Serialize(stream2, notarget);
                send(stream2, ((ManagerConnect)active_connections[active_index]).getSock_control());

                label5.Text = "Disconnesso";
                pictureBox1.Visible = false;
                pictureBox2.Visible = true;
                FullscreenForm.Hide();
                active_index = -1;
            }
            catch (Exception)
            {
                this.Close();
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            ClientToServ = e.KeyData.ToString();
            if (ClientToServ.Contains(","))
            {
                textBox1.Text = ClientToServ;
            }
            else
            {
                textBox1.Text = "";
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            servToClient = e.KeyData.ToString();
            if (servToClient.Contains(","))
            {
                textBox2.Text = servToClient;
            }
            else
            {
                textBox2.Text = "";
            }
        }

        public void InputError(String server_name)
        {
            ListViewItem tmp = null;
            tmp = serverList.FindItemWithText(server_name);

            try
            {
                if (tmp != null)
                {
                    int index = tmp.Index;
                    ManagerConnect selected_connection = (ManagerConnect)active_connections[index];
                    ((ManagerConnect)active_connections[index]).deactivateDelegate();

                    closeConnectionWithServer(selected_connection);
                    active_connections.RemoveAt(index);
                    serverList.Items[index].Remove();

                    label5.Text = "Disconnesso";
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = true;
                    FullscreenForm.WindowState = FormWindowState.Minimized;
                    FullscreenForm.Hide();
                    form_transfer.Hide();
                    if (index == active_index)
                    {
                        active_index = -1;
                    }
                }
            }
            catch(Exception )
            {
                this.Close();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (isTransferring == true)
                {
                    DialogResult dialogResult = MessageBox.Show("Trasferimento file in corso, forzare la chiusura?", "Attenzione!", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        e.Cancel = true;
                        return;
                    }

                    setIsTransferring(false, null);
                }

                for (int i = 0; i < active_connections.Count; i++)
                {
                    int selected_index = i;

                    ManagerConnect selected_connection = (ManagerConnect)active_connections[selected_index];
                    String target = "disconnect";
                    Stream stream = new MemoryStream();
                    XmlSerializer xs = new XmlSerializer(typeof(String));
                    xs.Serialize(stream, target);
                    send(stream, selected_connection.getSock_control());
                    closeConnectionWithServer(selected_connection);
                    Console.WriteLine("Connessione chiusa col server " + active_index.ToString());

                }

                active_connections.Clear();
                Application.Exit();
            }
            catch (Exception)
            {
                this.Close();
            }

            return;
        }

        public int send(Stream stream, Socket sock)
        {
            int actual_send = 0, res;
            byte[] bufferbytes;

            try
            {
                bufferbytes = ((MemoryStream)stream).ToArray();
                res = send_int(sock, bufferbytes.Length);
                if (res < 0)
                    return -1;

                while (actual_send != bufferbytes.Length)
                {
                    actual_send += sock.Send(bufferbytes, actual_send, bufferbytes.Length - actual_send, 0);
                }
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                
                Console.WriteLine(ex.Message);
                return -1;
            }

            return actual_send;
        }

        public int send_int(Socket sock, int n)
        {
            byte[] bufn = new byte[4];
            int res;

            try
            {
                bufn = BitConverter.GetBytes(n);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bufn);
                }
                res = sock.Send(bufn, 4, 0);
            }
            catch (Exception)
            {           
                return -1;
            }
            return res;
        }

    }
}
