﻿using ProgeetoGUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProgettoGUI
{
    public partial class Form2 : Form
    {
        public ProgeetoGUI.Form1 form1;
        ManagerConnect connection;
        public KeyEventArgs DisconnectHotkey;
        public KeyEventArgs Minimize;
        public String sDisconnectHotkey;
        public String sMinimize = "M, Control";
        public String sStandby = "S, Control";
        public String sClientToServ = "C, Alt";
        public String sServToClient = "S, Alt";
        
        

        public Form2()
        {
            InitializeComponent();
            this.FormClosing += this.Form2_FormClosing;// per non farlo chiudere ma solo nasconderlo
        }
        

        private void Form2_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            label4.Text = "utilizza \"" + sMinimize + "\" per minimizzare";
            form1.StartHook();
        }

        

        private void Form2_FormClosing(object sender, FormClosingEventArgs e) 
        {

            form1.label5.Text = "Disconnesso";
            form1.pictureBox1.Visible = false;
            form1.pictureBox2.Visible = true;
            this.WindowState = FormWindowState.Minimized;
            e.Cancel = true;    // Do not close the form.
        }


        public void SetConnection(ManagerConnect connection)
        {
            this.connection = connection;
        }

        public void ResetConnection() 
        {
            this.connection = null;
        }

        public void SetPointer(ProgeetoGUI.Form1 form1)
        {
            this.form1 = form1;
        }
        public void SetDisconnectHotkey(KeyEventArgs DisconnectHotkey)
        {
            this.DisconnectHotkey = DisconnectHotkey;
            this.sDisconnectHotkey = DisconnectHotkey.KeyData.ToString();
        }

        public void SetminimizeHotkey(KeyEventArgs Minimize)
        {
            this.Minimize = Minimize;
            this.sMinimize = Minimize.KeyData.ToString();
        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {

                if (sClientToServ != null)
                {
                    if (sClientToServ.CompareTo(e.KeyData.ToString()) == 0)
                    {

                        Console.WriteLine("clienttoserv eventargs!!!!!");

                        connection.block_event.Set();

                    }
                }

                if (sServToClient != null)
                {
                    if (sServToClient.CompareTo(e.KeyData.ToString()) == 0)
                    {

                        Console.WriteLine("servertoclient eventargs!!!!!");

                        String notarget = "send clipboard";
                        Stream stream2 = new MemoryStream();
                        XmlSerializer xs;
                        xs = new XmlSerializer(typeof(String));
                        xs.Serialize(stream2, notarget);
                        form1.send(stream2, ((ManagerConnect)form1.active_connections[form1.active_index]).getSock_control());
                    }
                }

                if (sMinimize != null)
                {
                    if (sMinimize.CompareTo(e.KeyData.ToString()) == 0)
                    {
                        Console.WriteLine("sono il form2 hai premuto " + e.KeyData.ToString() + " per minimizzare il form");
                        if (this.WindowState == FormWindowState.Maximized)
                        {
                            this.WindowState = FormWindowState.Minimized;
                        }
                        else if (this.WindowState == FormWindowState.Minimized)
                        {
                            this.WindowState = FormWindowState.Maximized;
                        }
                    }
                }

                if (sStandby != null)
                {
                    if (sStandby.CompareTo(e.KeyData.ToString()) == 0)
                    {
                        Console.WriteLine("sono il form2 hai premuto " + e.KeyData.ToString() + " per mettere in standby");
                        form1.standby_func();
                    }

                }
            }
            catch(Exception)
            {
                return;
            }
        }

        private void Form2_Resize(object sender, EventArgs e)
        {
            Console.WriteLine("resize " + WindowState);
            label4.Text = "utilizza \"" + sMinimize + "\" per minimizzare";

            if (WindowState == FormWindowState.Normal || WindowState == FormWindowState.Maximized)
            {
                Console.WriteLine("Normal");
                form1.StartHook();
            }
            else
            {
                form1.StopHook();

                sendUp(162); //ctrl-sx up
                sendUp(163); //ctrl-dx up
                sendUp(164); //alt up
                sendUp(160); //shift-sx up
                sendUp(161); //shift-dx up
            }
        }

        private void sendUp(short keycode)
        {
            try
            {
                InputHook.KBLLHOOKSTRUCT input = new InputHook.KBLLHOOKSTRUCT();
                input.time = 0; //necessario per non fare attivare lo screensaver
                input.flags = 0X2;
                input.vkCode = keycode;

                InputHook.INPUT inputmsg = new InputHook.INPUT();
                inputmsg.type = 1;
                inputmsg.ki = input;

                Stream stream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer((new InputHook.INPUT()).GetType()); ;
                xs.Serialize(stream, inputmsg);
                form1.send(stream, ((ManagerConnect)form1.active_connections[form1.active_index]).getSock_input());
                
            }
            catch(Exception )
            {
                Console.WriteLine("Errore; chiusura form2");      
                return;
            }
        }
           
    }
}
