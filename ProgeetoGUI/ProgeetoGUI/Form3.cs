﻿using ProgeetoGUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProgettoGUI
{
    public partial class Form3 : Form
    {
        public ManagerConnect connection;
        public Form1 form;
        Socket sock_conn;

        public Form3(Socket sock)
        {
            InitializeComponent();
            sock_conn = sock;
            this.FormClosed += this.Form3_FormClosed;
        }

       
        public void SetConnection(ManagerConnect connection)
        {
            this.connection = connection;
        }

        public void SetForm(Form1 form)
        {
            this.form = form;
        }

        private void button1_Click(object sender, EventArgs e)//connetti
        {

            XmlSerializer xs = null;
            int res;
            Console.WriteLine("Login....");

            try
            {
                string password = textBox1.Text;
                byte[] b_password = Encoding.UTF8.GetBytes(password);

                byte[] salt = new byte[2] { 1, 1 };
                byte[] hash = GenerateSaltedHash(b_password, salt);

                if (!string.IsNullOrEmpty(password))
                {
                        res = connection.send_int(sock_conn, 2);
                        if (res == -1)
                            throw new Exception();
                        string serverReply;
                        res = connection.send_int(sock_conn, hash.Length);
                        if (res == -1)
                            throw new Exception();

                        try
                        {
                            sock_conn.Send(hash, hash.Length, 0);
                        }
                        catch (Exception ) { throw new SocketException(); }

                        Stream stream1 = connection.receive(sock_conn);
                        if(stream1 == null)
                            throw new SocketException();

                        try
                        {
                            xs = new XmlSerializer(typeof(string));
                        }
                        catch (InvalidOperationException ) { throw new Exception(); }
                        try
                        {
                            serverReply = (string)xs.Deserialize(stream1);
                        }
                        catch (InvalidOperationException ) { throw new Exception(); }

                        if (serverReply.CompareTo("ok") == 0)
                        {
                            Console.WriteLine("Password correct");
                            textBox1.Text = "";

                            int w, h;
                            w = Screen.PrimaryScreen.Bounds.Width;
                            h = Screen.PrimaryScreen.Bounds.Height;
                            Console.WriteLine("W: " + w.ToString() + ", H: " + h.ToString());
                            ScreenSize SS = new ScreenSize(w, h);

                            try
                            {
                                xs = new XmlSerializer(typeof(ScreenSize));
                            }
                            catch (InvalidOperationException ) { throw new Exception(); }

                            Stream stream3 = new MemoryStream();
                            try
                            {
                                xs.Serialize(stream3, SS);
                            }
                            catch (InvalidOperationException ) { throw new Exception(); }

                            res = connection.send(stream3, sock_conn);
                            if(res == -1)
                                throw new Exception();

                            this.Hide();
                            form.connetti();

                        }
                        else
                        {
                            Console.WriteLine("Invalid password!");
                            MessageBox.Show("Password Errata");
                            textBox1.Text = "";
                            return;
                        }
                    }
                else
                {
                    Console.WriteLine("Please insert a valid password");
                    MessageBox.Show("Please insert a valid password", "Server Reply",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (SocketException se)
            {
                Console.WriteLine("Errore form 3:" + se.Message);
                form.closeConnectionWithServer(connection);
                MessageBox.Show("Connessione fallita", "Server Reply",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            catch (InvalidOperationException ioe)
            {
                Console.WriteLine("Errore form 3" + ioe.Message);
                form.closeConnectionWithServer(connection);
                MessageBox.Show("Connessione fallita", "Server Reply",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            catch (Exception) 
            {
                Console.WriteLine("Errore form 3");
                form.closeConnectionWithServer(connection);
                MessageBox.Show("Connessione fallita", "Server Reply",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;    // Do not close the form.

        }

        static byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            HashAlgorithm algorithm;

            byte[] plainTextWithSaltBytes,res;
              
            try
            {
                algorithm = new SHA256Managed();
                plainTextWithSaltBytes = new byte[plainText.Length + salt.Length];

                for (int i = 0; i < plainText.Length; i++)
                {
                    plainTextWithSaltBytes[i] = plainText[i];
                }
                for (int i = 0; i < salt.Length; i++)
                {
                    plainTextWithSaltBytes[plainText.Length + i] = salt[i];
                }
                res = algorithm.ComputeHash(plainTextWithSaltBytes);
            }
            catch (Exception ) { return null; }

            return res;
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            int res;

            //Operazione di login interrotta
            res = connection.send_int(sock_conn, 1);
            form.closeConnectionWithServer(connection);
        }

    }
}
