﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProgettoGUI
{
    public class InputHook
    {
        private const int WH_MOUSE_LL = 14;
        private const int WH_KEYBOARD_LL = 13;

        private enum MouseMessages
        {
            WM_LBUTTONDOWN = 0x0201,
            WM_LBUTTONUP = 0x0202,
            WM_MOUSEMOVE = 0x0200,
            WM_MOUSEWHEEL = 0x020A,
            WM_RBUTTONDOWN = 0x0204,
            WM_RBUTTONUP = 0x0205
        }

        private enum KeyboardMessages
        {
            WM_KEYDOWN = 0x100,
            WM_KEYUP = 0x101,
            WM_SYSKEYDOWN = 0x104,
            WM_SYSKEYUP = 0x105,
            VK_SHIFT = 0x10,
            VK_CAPITAL = 0x14,
            VK_NUMLOCK = 0x90
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct INPUT
        {
            [FieldOffset(0)]
            public int type;
            [FieldOffset(4)]
            public MSLLHOOKSTRUCT mi;
            [FieldOffset(4)]
            public KBLLHOOKSTRUCT ki;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MSLLHOOKSTRUCT
        {
            public int dx;
            public int dy;
            public int mouseData;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct KBLLHOOKSTRUCT
        {
            public short vkCode;
            public short scanCode;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
          LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
          LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
          IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);


        private static LowLevelMouseProc _proc = MouseHookCallback;
        private static LowLevelKeyboardProc _proc2 = KeyboardHookCallback;

        private static IntPtr _hookID = IntPtr.Zero;
        private static IntPtr _hookID2 = IntPtr.Zero;

        public delegate void MouseStructSender(MSLLHOOKSTRUCT MouseStruct);
        public delegate void KeyboardStructSender(KBLLHOOKSTRUCT KeyboardStruct);
        public static MouseStructSender MouseAction;
        public static KeyboardStructSender KeyboardAction;

        private delegate IntPtr LowLevelMouseProc(int nCode, IntPtr wParam, IntPtr lParam);
        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        public static void Start()
        {
            _hookID = SetMouseHook(_proc);
            _hookID2 = SetKeyboardHook(_proc2);
        }
        public static void Stop()
        {
            UnhookWindowsHookEx(_hookID);
            UnhookWindowsHookEx(_hookID2);
        }


        private static IntPtr SetMouseHook(LowLevelMouseProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                IntPtr hook = SetWindowsHookEx(WH_MOUSE_LL, proc, GetModuleHandle("user32"), 0);
                if (hook == IntPtr.Zero) throw new System.ComponentModel.Win32Exception();
                return hook;
            }
        }

        private static IntPtr SetKeyboardHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                IntPtr hook = SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle("user32"), 0);
                if (hook == IntPtr.Zero) throw new System.ComponentModel.Win32Exception();
                return hook;
            }
        }

        private static IntPtr MouseHookCallback(
          int nCode, IntPtr wParam, IntPtr lParam)
        {
            try
            {
                if (nCode >= 0)
                {
                    MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
                    if (wParam.ToInt32() == 0X200) //Mouse Move
                    {
                        hookStruct.flags = 0X1 | 0X8000; //Move absolute
                    }
                    if (wParam.ToInt32() == 0x201) //Mouse left button down
                    {
                        hookStruct.flags = 0x2 | 0x8000;
                    }
                    if (wParam.ToInt32() == 0x202) //Mouse left button up
                    {
                        hookStruct.flags = 0x4 | 0x8000;
                    }
                    if (wParam.ToInt32() == 0x204) //Mouse right button down
                    {
                        hookStruct.flags = 0x8 | 0x8000;
                    }
                    if (wParam.ToInt32() == 0x205) //Mouse right button up
                    {
                        hookStruct.flags = 0x10 | 0x8000;
                    }
                    if (wParam.ToInt32() == 0x20A) //Mouse Wheel
                    {
                        hookStruct.flags = 0x800;
                        hookStruct.mouseData = hookStruct.mouseData >> 16;
                    }
                    MouseAction(hookStruct);
                }
            }
            catch (Exception e) { Console.WriteLine(e.Message); }

            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        private static IntPtr KeyboardHookCallback(
  int nCode, IntPtr wParam, IntPtr lParam)
        {
            try
            {
                if ((nCode >= 0))
                {
                    KBLLHOOKSTRUCT hookStruct = (KBLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBLLHOOKSTRUCT));
                    if (wParam.ToInt32() == 0X100 || wParam.ToInt32() == 0x104) //keyboard button down
                    {
                        hookStruct.flags = 0X0; //key down
                        if (hookStruct.vkCode == 91 || hookStruct.vkCode == 92)
                        {
                            KeyboardAction(hookStruct);
                            return (System.IntPtr)1;
                        }
                    }
                    else if (wParam.ToInt32() == 0X101 || wParam.ToInt32() == 0X105) //keyboard button up
                    {
                        hookStruct.flags = 0X2; //key up
                        if (hookStruct.vkCode == 91 || hookStruct.vkCode == 92)
                        {
                            KeyboardAction(hookStruct);
                            return (System.IntPtr)1;
                        }
                    }
                    KeyboardAction(hookStruct);
                }
            }
            catch (Exception e) { Console.WriteLine(e.Message); }
            return CallNextHookEx(_hookID2, nCode, wParam, lParam);
        }
    }
}
