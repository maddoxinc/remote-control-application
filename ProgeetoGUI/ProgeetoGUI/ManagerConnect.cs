﻿using ProgeetoGUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProgettoGUI
{
    public class ManagerConnect
    {
        public Thread thread_control;
        public Thread thread_clipboard;
        public Thread thread_recClipboard;
        public Socket sock_control;
        public Socket sock_input;
        public Socket sock_clipboard;
        public Socket sock_recClipboard;
        String server_name;
        String server_addr;
        int control_port;
        Form3 Login;
        public Form2 FullScreenForm;
        Form1 controlForm;
        public Worker worker_clipboard;
        Receive_clipboard worker_recClipboard;
        XmlSerializer xs;
        public AutoResetEvent block_event;
        InputHook.MouseStructSender mss;
        InputHook.KeyboardStructSender kss;
        public String shortcut;

        public ManagerConnect(String server_name, String server_addr,int control_port, Form2 FullScreenForm, Form1 controlForm, String shortcut)
        {
            try
            {
                this.controlForm = controlForm;
                sock_control = connect("control", server_addr, control_port);
                if (sock_control == null)
                {
                    MessageBox.Show("Impossibile stabilire una connessione con il server", "Errore",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw new SocketException();
                }
                this.server_name = server_name;
                this.server_addr = server_addr;
                this.control_port = control_port;
                this.FullScreenForm = FullScreenForm;
                this.shortcut = shortcut;

                block_event = new AutoResetEvent(false);
                Login = new Form3(sock_control);
                Login.SetConnection(this);
                Login.SetForm(controlForm);

                //Inserire Login
                Login.Show();
            }
            catch(SocketException se)
            {
                Console.WriteLine(se.Message);
                return;
            }
           
        }

        public void setControlThread(Thread thread_control)
        {
            this.thread_control = thread_control;
        }

        public void connetti()
        {
            try
            {
                sock_input = connect("input", server_addr, control_port + 1);
                if (sock_input == null)
                {
                    MessageBox.Show("Impossibile stabilire una connessione con il server", "Errore",
                             MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw new SocketException();
                }

                mss = new InputHook.MouseStructSender(MSEvent);
                kss = new InputHook.KeyboardStructSender(KBEvent);
                activateDelegate();

                sock_clipboard = connect("clipboard", server_addr, control_port + 2);
                if (sock_clipboard == null)
                {
                    throw new SocketException();
                }

                worker_clipboard = new Worker(sock_clipboard, this, block_event, controlForm);
                thread_clipboard = new Thread(worker_clipboard.DoWork);
                thread_clipboard.SetApartmentState(ApartmentState.STA);

                // Start the worker thread.
                thread_clipboard.Start();

                sock_recClipboard = connect("rec clipboard", server_addr, control_port + 3);
                if (sock_recClipboard == null)
                {
                    throw new SocketException();
                }

                worker_recClipboard = new Receive_clipboard(sock_recClipboard, this, FullScreenForm, controlForm);
                thread_recClipboard = new Thread(worker_recClipboard.DoWork);
                thread_recClipboard.SetApartmentState(ApartmentState.STA);

                // Start the worker thread.
                thread_recClipboard.Start();

                //show fullscreenForm
                controlForm.Invoke((MethodInvoker)delegate()
                {
                    FullScreenForm.WindowState = FormWindowState.Maximized;
                    FullScreenForm.Show();
                });

                String target = "target";
                Stream stream = new MemoryStream();
               
                try
                {
                    xs = new XmlSerializer(typeof(String));
                    xs.Serialize(stream, target);
                }
                catch (InvalidOperationException ) { throw new Exception(); }
                
                
                int res = send(stream, sock_control);
                if(res == 1)
                    throw new SocketException();
                try
                {
                    xs = new XmlSerializer((new InputHook.INPUT()).GetType());
                }
                catch (InvalidOperationException ) { throw new Exception(); }
                
            }
            catch(SocketException se)
            {
                Console.WriteLine(se.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }

            return;
        }

        public void activateDelegate()
        {

            InputHook.MouseAction += mss;
            InputHook.KeyboardAction += kss;
        }

        public void deactivateDelegate()
        {
            InputHook.MouseAction -= mss;
            InputHook.KeyboardAction -= kss;
        }

        public void standby_connection()
        {
            Console.WriteLine("Entrato!!!");
            block_event.WaitOne();
            reactivate_connection();
        }

        private void reactivate_connection()
        {

        }

        public string getName()
        {
            return server_name;
        }

        public int getPort()
        {
            return control_port;
        }

        public string getAddress()
        {
            return server_addr;
        }

        public Socket getSock_control()
        {
            return sock_control;
        }

        public Socket getSock_input()
        {
            return sock_input;
        }


        public void MSEvent(InputHook.MSLLHOOKSTRUCT input)
        {
            try
            {
                int res;
                input.time = 0; //necessario per non fare attivare lo screensaver
                InputHook.INPUT inputmsg = new InputHook.INPUT();
                inputmsg.type = 0;
                inputmsg.mi = input;
               

                Stream stream = new MemoryStream();
                xs.Serialize(stream, inputmsg);
                res = send(stream, sock_input);
                if (res < 0)
                {
                    controlForm.Invoke((MethodInvoker)delegate()
                    {
                        controlForm.InputError(this.server_name);
                    });
                    return;
                }
            }
            catch (SocketException se)
            {
                Console.WriteLine(se.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }
            catch (InvalidOperationException ioe)
            {
                Console.WriteLine(ioe.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }
        }


        public void KBEvent(InputHook.KBLLHOOKSTRUCT input)
        {
            try
            {
                int res;
                input.time = 0; //necessario per non fare attivare lo screensaver
                InputHook.INPUT inputmsg = new InputHook.INPUT();
                inputmsg.type = 1;
                inputmsg.ki = input;

                Stream stream = new MemoryStream();
                xs.Serialize(stream, inputmsg);
                res = send(stream, sock_input);
                if (res < 0)
                {
                    controlForm.Invoke((MethodInvoker)delegate()
                    {
                        controlForm.InputError(this.server_name);
                    });
                    
                    return;
                }
            }
            catch (SocketException se)
            {
                Console.WriteLine(se.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }
            catch (InvalidOperationException ioe)
            {
                Console.WriteLine(ioe.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                worker_clipboard.block_event.Set();
                thread_clipboard.Join();
                thread_recClipboard.Join();
                Console.WriteLine("chiusura Connection!");
                return;
            }
        }

        private Socket connect(String conn_name, String address, int port)
        {
            SocketPermission sockperm;
            Socket sock;
            IPEndPoint iep;

            try
            {
                sockperm = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, address, port);
                sockperm.Demand();

                IPAddress ipa = IPAddress.Parse(address);
                iep = new IPEndPoint(ipa, port);
                sock = null;
                try
                {
                    sock = new Socket(iep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                }
                catch (SystemException )
                {
                    return null;
                }

                try
                {
                    sock.Connect(iep);
                }
                catch (System.Net.Sockets.SocketException)
                {
                    return null;
               }
            }
            catch (Exception ) { return null; }

            return sock;
        }

        public int send(Stream stream, Socket sock)
        {
            int actual_send = 0, res;
            byte[] bufferbytes;

            try
            {
                bufferbytes = ((MemoryStream)stream).ToArray();
                res = send_int(sock, bufferbytes.Length);
                if (res < 0)
                    return -1;

                while (actual_send != bufferbytes.Length)
                {
                    actual_send += sock.Send(bufferbytes, actual_send, bufferbytes.Length - actual_send, 0);
                }
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }

            return actual_send;
        }


        public Stream receive(Socket connsock)
        {
            byte[] bufferbyter;
            Stream stream1;

            try
            {
                int bytetoread = receive_int(connsock);
                if (bytetoread == -1)
                    return null;
                bufferbyter = new byte[bytetoread];

                if (!receive_checked(connsock, ref bufferbyter, bytetoread))
                    return null;

                stream1 = new MemoryStream(bufferbyter);
            }
            catch (Exception )
            {
                return null;
            }

            
            return stream1;
        }

        public int receive_int(Socket sock)
        {
            byte[] bufn = new byte[4];
            int res;
            try
            {
                if (!receive_checked(sock, ref bufn, 4))
                    return -1;
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bufn);
                }

                res = BitConverter.ToInt32(bufn, 0);
            }
            catch (Exception ){ return -1; }

            return res;
        }

        public bool receive_checked(Socket sock, ref byte[] buffer, int n)
        {
            int actual_read = 0;

            try
            {
                while (actual_read != n)
                {
                    actual_read += sock.Receive(buffer, actual_read, n - actual_read, 0);
                    if (actual_read < 0)
                        return false;
                }
            }
            catch (Exception ) { return false; }
     
            return true;
        }

        public int send_int(Socket sock, int n)
        {
            byte[] bufn = new byte[4];
            int res;

            try
            {
                bufn = BitConverter.GetBytes(n);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bufn);
                }
                res = sock.Send(bufn, 4, 0);
            }
            catch (Exception )
            {
                return -1;
            }
            return res;
        }
    }
}
