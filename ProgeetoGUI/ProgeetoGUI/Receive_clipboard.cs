﻿using ProgeetoGUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProgettoGUI
{
    class Receive_clipboard
    {
        public Socket connsock;
        private ManagerConnect connection;
        Form2 fullscreenform;
        Form1 control_form;
        public AutoResetEvent transfer_event;
        public delegate void transfer_delegate(bool flag, AutoResetEvent ev);
        Object[] array_obj;

        public Receive_clipboard(Socket connsock, ManagerConnect connection, Form2 fullscreenform, Form1 control_form)
        {
            this.connsock = connsock;
            this.connection = connection;
            this.fullscreenform = fullscreenform;
            this.control_form = control_form;
            this.transfer_event = new AutoResetEvent(false);
            array_obj = new Object[2];
            array_obj[0] = new Boolean();
            array_obj[1] = transfer_event;
        }

        public void DoWork()
        {
            byte[] bufferbyter = new byte[1];
            byte[] bufferbyterr = new byte[1024];
            byte[] bufferl = new byte[4];
            bool flag = true;
            int res;

            while (flag)
            {
                try
                {
                    Stream stream1 = connection.receive(connsock);
                    if (stream1 == null)
                        throw new SocketException();

                    String format = receive_format(stream1);
                    if (format == null)
                        throw new SocketException();

                    if (format == DataFormats.Bitmap)
                    {
                        XmlSerializer clipSer = new XmlSerializer(typeof(Bitmap));
                        stream1 = connection.receive(connsock);
                        Bitmap bmp = new Bitmap(stream1);
                        Console.WriteLine("clipboard data receveid: Bitmap\n");
                        Clipboard.SetDataObject(bmp, true);
                    }
                    else if (format == DataFormats.Rtf)
                    {
                        XmlSerializer clipSer = new XmlSerializer(typeof(String));
                        stream1 = connection.receive(connsock);
                        string data = (string)clipSer.Deserialize(stream1);
                        Console.WriteLine("clipboard data receveid rtf\n" + data);
                        Clipboard.SetData(DataFormats.Rtf, data.Replace("\n", "\r\n"));
                    }
                    else if (format == DataFormats.Text)
                    {
                        XmlSerializer clipSer = new XmlSerializer(typeof(String));
                        stream1 = connection.receive(connsock);
                        string data = (string)clipSer.Deserialize(stream1);
                        Console.WriteLine("clipboard data receveid:\n" + data);
                        Clipboard.SetData(DataFormats.Text, data.Replace("\n", "\r\n"));
                    }
                    else if (format == DataFormats.FileDrop)
                    {
                        array_obj[0] = true;
                        control_form.Invoke(new transfer_delegate(control_form.setIsTransferring), array_obj);
                        transfer_event.WaitOne();
                        Console.WriteLine("Filedrop!");

                        String sel_path;
                        int elements;

                        FolderBrowserDialog dialog1 = new FolderBrowserDialog();
                        dialog1.Description = "Choose destination folder";
                        
                        if (dialog1.ShowDialog() == DialogResult.OK)
                        {
                            res = control_form.send_int(connsock, 3);
                            if (res == -1)
                                throw new Exception();

                            control_form.Invoke((MethodInvoker)control_form.showFormTransfer);
                            sel_path = dialog1.SelectedPath;

                            //Add trailing "//" if necessary
                            if (sel_path[sel_path.Length - 1] != '\\')
                            {
                                sel_path = sel_path + "\\";
                            }

                            elements = connection.receive_int(connsock);
                            if (elements == -1)
                                throw new SocketException();

                            create_structure(sel_path, elements);

                            array_obj[0] = false;
                            control_form.Invoke(new transfer_delegate(control_form.setIsTransferring), array_obj);
                            transfer_event.WaitOne();
                            control_form.Invoke((MethodInvoker)control_form.hideFormTransfer);
                        }
                        else
                        {
                            //blocca il trasferimento
                            res = control_form.send_int(connsock, 2);
                            if (res == -1)
                                throw new Exception();
                            array_obj[0] = false;
                            control_form.Invoke(new transfer_delegate(control_form.setIsTransferring), array_obj);
                            transfer_event.WaitOne();
                            //form.textbox5.AppendText("Operazione cancellata o fallita\n");
                            Console.WriteLine("Operazione cancellata o fallita");
                        }
                    }
                    else
                    {
                        //formato sconosciuto
                    }
                }
                catch (SocketException)
                {
                    Console.WriteLine("chiusura receive_clipboard");
                    flag = false;
                }
                catch (InvalidOperationException ex)
                {

                    Console.WriteLine("chiusura receive_clipboard " + ex.Message);
                    flag = false;
                }
                catch (Exception)
                {
                    Console.WriteLine("chiusura generica receive_clipboard");
                    flag = false;
                }
            }
            
            return;
        }

        private string receive_format(Stream stream)
        {
            XmlSerializer xs = new XmlSerializer(typeof(String));

            return (String)xs.Deserialize(stream);
        }

        public void create_structure(String path, int elements)
        {
            int subelements;

            for (int i = 0; i < elements; i++)
            {
                String dirname = null;
                receive_file_directory(connsock, path, ref dirname);

                if (dirname == null) //File
                {

                }
                else //Directory
                {
                    subelements = connection.receive_int(connsock);

                    create_structure(path + dirname + "\\", subelements);
                }
            }
        }

        private bool receive_file_directory(Socket sock, String path, ref String dirname)
        {
            byte[] buffer = new byte[1400];
            int bytetoread;
            String fullname;

            //Receive name
            bytetoread = connection.receive_int(sock);

            connection.receive_checked(sock, ref buffer, bytetoread);

            fullname = path + "" + Encoding.Unicode.GetString(buffer, 0, bytetoread);

            //Receive code (file or directory)
            int isFile = connection.receive_int(sock);

            if (isFile == 1)
            {
                dirname = null;
                Console.WriteLine("Receiving file " + fullname);
                FileStream filestream = null;

                if (File.Exists(fullname))
                {
                    File.WriteAllText(fullname, string.Empty);
                }

                try
                {
                    filestream = new FileStream(fullname, FileMode.OpenOrCreate, FileAccess.Write);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
                int nread = -1;

                while (nread != 0)
                {
                    try
                    {
                        bytetoread = connection.receive_int(sock);

                        if (bytetoread == -2)
                        {
                            Console.WriteLine("Error in client (wrong file?)");
                            return false;
                        }
                        if (bytetoread == -5)
                        {
                            break;
                        }

                        connection.receive_checked(sock, ref buffer, bytetoread);

                        filestream.Write(buffer, 0, bytetoread);
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                        return false;
                    }
                    catch (System.Net.Sockets.SocketException e)
                    {
                        Console.WriteLine(e.Message);
                        return false;
                    }
                }

                filestream.Close();
                Console.WriteLine("File received!");
                return true;
            }
            else if (isFile == 2) //Directory
            {
                String[] parts = fullname.Split('\\');
                dirname = parts[parts.Length - 1];
                Console.WriteLine("Receiving directory " + fullname);
                Directory.CreateDirectory(fullname);
                return true;
            }
            else
            {
                Console.WriteLine("Error in code!");
                return false;
            }
        }

        private bool send_file_directory(Socket sock, String name, String fullname, bool isFile)
        {
            byte[] buffer;
            int buffer_len = 1400;

            //Send name
            buffer = Encoding.Unicode.GetBytes(name);
            connection.send_int(sock, buffer.Length);
            sock.Send(buffer, buffer.Length, 0);

            if (isFile)
            {
                FileStream filestream = null;
                buffer = new byte[1400];
                Console.WriteLine("Sending file " + fullname);

                //A file is about to be sent
                connection.send_int(sock, 1);

                try
                {
                    filestream = new FileStream(fullname, FileMode.Open, FileAccess.Read);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                    connection.send_int(sock, -2);
                    return false;
                }
                int nread = -1;

                while (nread != 0)
                {
                    try
                    {
                        nread = filestream.Read(buffer, 0, buffer_len);
                        if (nread == 0)
                        {
                            break;
                        }
                        connection.send_int(sock, nread);
                        sock.Send(buffer, nread, 0);
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                        return false;
                    }
                    catch (System.Net.Sockets.SocketException e)
                    {
                        Console.WriteLine(e.Message);
                        return false;
                    }
                }

                connection.send_int(sock, -5);

                filestream.Close();
                Console.WriteLine("File Sent!");
                return true;
            }
            else //Directory
            {
                //A directory is about to be sent
                Console.WriteLine("Communicating directory " + fullname);
                connection.send_int(sock, 2);

                return true;
            }
        }

    }
}
