﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoGUI
{
    [Serializable]
    public class ScreenSize
    {
        public int width, height;

        public ScreenSize() { }

        public ScreenSize(int w, int h)
        {
            this.width = w;
            this.height = h;
        }
    }
}
