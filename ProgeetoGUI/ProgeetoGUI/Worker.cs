﻿using ProgeetoGUI;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ProgettoGUI
{
    public class Worker
    {
        public Socket sock_clip;
        public Mutex m;
        public ManagerConnect connection;
        public AutoResetEvent block_event;
        public AutoResetEvent transfer_event;
        public delegate void transfer_delegate(bool flag, AutoResetEvent ev);
        Object[] array_obj;
        Form1 form1;

        public Worker(Socket sock, ManagerConnect connection, AutoResetEvent ev, Form1 form1)
        {
            this.block_event = new AutoResetEvent(false);
            this.transfer_event = new AutoResetEvent(false);
            array_obj = new Object[2];
            array_obj[0] = new Boolean();
            array_obj[1] = transfer_event;
            this.block_event = ev;
            this.connection = connection;
            sock_clip = sock;
            this.form1 = form1;
        }

        public void DoWork()
        {

            bool flag = true;
            int res;

            while (flag)
            {
                try
                {

                    block_event.WaitOne();
                    if (connection.sock_clipboard == null)
                        throw new SocketException();

                    XmlSerializer clipSer = null;

                    string myStr = null;
                    DataObject retrievedData = (DataObject)Clipboard.GetDataObject();

                    if (retrievedData.GetDataPresent(DataFormats.Rtf))
                    {
                        //rtf
                        //Console.WriteLine("rtf!!!!");

                        Stream tmp = send_format(DataFormats.Rtf);
                        if (tmp == null)
                            throw new SocketException();

                        myStr = (String)Clipboard.GetData(DataFormats.Rtf);
                        try
                        {
                            clipSer = new XmlSerializer(myStr.GetType());
                        }
                        catch (System.InvalidOperationException)
                        {
                            throw new Exception();
                        }

                        Stream stream = new MemoryStream();
                        try
                        {
                            clipSer.Serialize(stream, myStr);
                        }
                        catch (InvalidOperationException)
                        {
                            throw new Exception();
                        }
                        res = connection.send(stream, sock_clip);
                        if (res < 0)
                            throw new SocketException();

                    }
                    else if (retrievedData.GetDataPresent(DataFormats.Text))
                    {
                        // Console.WriteLine("text!!!!");

                        Stream tmp = send_format(DataFormats.Text);
                        if (tmp == null)
                            throw new SocketException();

                        myStr = Clipboard.GetText();
                        try
                        {
                            clipSer = new XmlSerializer(myStr.GetType());
                        }
                        catch (System.InvalidOperationException)
                        {
                            throw new Exception();
                        }

                        Stream stream = new MemoryStream();
                        try
                        {
                            clipSer.Serialize(stream, myStr);
                        }
                        catch (InvalidOperationException)
                        {
                            throw new Exception();
                        }
                        res = connection.send(stream, sock_clip);
                        if (res < 0)
                            throw new SocketException();
                    }
                    else if (retrievedData.GetDataPresent(DataFormats.Bitmap))
                    {
                        //Console.WriteLine("bitmap!!!!");
                        Stream tmp = send_format(DataFormats.Bitmap);
                        if (tmp == null)
                            throw new SocketException();

                        byte[] bufferbytes, bufferl;
                        try
                        {
                            Bitmap myBmp = (Bitmap)Clipboard.GetData(DataFormats.Bitmap);
                            ImageConverter converter = new ImageConverter();

                            bufferbytes = (byte[])converter.ConvertTo(myBmp, typeof(byte[]));

                            bufferl = BitConverter.GetBytes(bufferbytes.Length);
                            if (BitConverter.IsLittleEndian)
                            {
                                Array.Reverse(bufferl);
                            }
                        }
                        catch (System.Net.Sockets.SocketException)
                        {
                            throw new Exception();
                        }

                        try
                        {
                            sock_clip.Send(bufferl, bufferl.Length, 0);
                            sock_clip.Send(bufferbytes, bufferbytes.Length, 0);
                        }
                        catch (Exception) { throw new SocketException(); }
                    }
                    else if (retrievedData.GetDataPresent(DataFormats.FileDrop))
                    {

                        array_obj[0] = true;
                        form1.Invoke(new transfer_delegate(form1.setIsTransferring), array_obj);
                        transfer_event.WaitOne();
                        Stream tmp = send_format(DataFormats.FileDrop);
                        if (tmp == null)
                            throw new SocketException();

                        /////////////aspetto che l'utente prema su ok nel FolderBrowserDialog

                        res = connection.receive_int(sock_clip);
                        if (res == -1)
                            throw new SocketException();

                        if (res == 3)
                        {
                            form1.Invoke((MethodInvoker)form1.showFormTransfer);
                            StringCollection sc = Clipboard.GetFileDropList();
                            int n = sc.Count;

                            //Send number of elements on top level
                            res = connection.send_int(sock_clip, n);
                            if (res < 0)
                                throw new SocketException();


                            for (int i = 0; i < n; i++)
                            {
                                FileAttributes attr = File.GetAttributes(sc[i]);

                                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                                {
                                    //Directory
                                    DirectoryInfo dir = new DirectoryInfo(sc[i]);
                                    DirectoryInfo[] subdirs = dir.GetDirectories();
                                    FileInfo[] subfiles = dir.GetFiles();
                                    if (!send_file_directory(sock_clip, dir.Name, dir.FullName, false))
                                        throw new SocketException();

                                    //Send number of elements on sub level
                                    res = connection.send_int(sock_clip, subdirs.Length + subfiles.Length);
                                    if (res == -1)
                                    {
                                        array_obj[0] = false;
                                        form1.Invoke(new transfer_delegate(form1.setIsTransferring), array_obj);
                                        transfer_event.WaitOne();
                                        form1.Invoke((MethodInvoker)form1.hideFormTransfer);
                                        throw new SocketException();
                                    }
                                        

                                    if (!detect_structure(subdirs, subfiles))
                                    {
                                        //form1.writeOnLog("Fine!\r\n");
                                        array_obj[0] = false;
                                        //form1.Invoke(new transfer_delegate(form1.setIsTransferring), array_obj);
                                        transfer_event.WaitOne();
                                        //form1.Invoke((MethodInvoker)form1.hideFormTransfer);
                                        throw new SocketException();
                                    }
                                }
                                else
                                {
                                    //File
                                    FileInfo fil = new FileInfo(sc[i]);
                                    if (!send_file_directory(sock_clip, fil.Name, fil.FullName, true))
                                        throw new SocketException();
                                }
                            }
                            array_obj[0] = false;
                            form1.Invoke(new transfer_delegate(form1.setIsTransferring), array_obj);
                            transfer_event.WaitOne();
                            form1.Invoke((MethodInvoker)form1.hideFormTransfer);
                            Console.WriteLine("Fine!");
                        }
                        else
                        {
                            Console.WriteLine("trasferimento annullato dall'utente");
                            array_obj[0] = false;
                            form1.Invoke(new transfer_delegate(form1.setIsTransferring), array_obj);
                            transfer_event.WaitOne();
                        }
                    }
                    else
                    {
                        //Console.WriteLine("altro formato!!!!");
                    }
                }
                catch (SocketException)
                {
                    Console.WriteLine("Chiusura worker");
                    flag = false;
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Chiusura worker");
                    flag = false;
                }
                catch (Exception)
                {
                    Console.WriteLine("Chiusura generica worker");
                    flag = false;
                }
            }
        }

        private Stream send_format(string format)
        {
            XmlSerializer clipSer = null;
            Stream stream;
            try
            {
                clipSer = new XmlSerializer(format.GetType());
                stream = new MemoryStream();
                clipSer.Serialize(stream, format);
                connection.send(stream, sock_clip);
            }
            catch (Exception)
            {
                return null;
            }

            return stream;
        }

        private bool detect_structure(DirectoryInfo[] dirs, FileInfo[] files)
        {
            int nd, nf, res;

            try
            {
                nd = dirs.Length;
                nf = files.Length;
                for (int i = 0; i < nd; i++)
                {
                    DirectoryInfo[] subdirs = dirs[i].GetDirectories();
                    FileInfo[] subfiles = dirs[i].GetFiles();
                    if (!send_file_directory(sock_clip, dirs[i].Name, dirs[i].FullName, false))
                        return false;

                    //Send number of elements on sub level
                    res = connection.send_int(sock_clip, subdirs.Length + subfiles.Length);
                    if (res == -1)
                        return false;
                    if (!detect_structure(subdirs, subfiles))
                        return false;
                }

                for (int i = 0; i < nf; i++)
                {
                    if (!send_file_directory(sock_clip, files[i].Name, files[i].FullName, true))
                        return false;
                }
            }
            catch (Exception) { return false; }

            return true;
        }

        private bool receive_file_directory(Socket sock, String path, ref String dirname)
        {
            byte[] buffer = new byte[1400];
            int bytetoread;

            String fullname;

            //Receive name
            try
            {
                bytetoread = connection.receive_int(sock);
                if (bytetoread == -1)
                    return false;

                if (connection.receive_checked(sock, ref buffer, bytetoread))
                    return false;

                fullname = path + "" + Encoding.Unicode.GetString(buffer, 0, bytetoread);

                //Receive code (file or directory)
                int isFile = connection.receive_int(sock);
                if (isFile == -1)
                    return false;

                if (isFile == 1)
                {
                    dirname = null;
                    //Console.WriteLine("Receiving file " + fullname);
                    FileStream filestream = null;

                    if (File.Exists(fullname))
                    {
                        File.WriteAllText(fullname, string.Empty);
                    }

                    try
                    {
                        filestream = new FileStream(fullname, FileMode.OpenOrCreate, FileAccess.Write);
                    }
                    catch (System.IO.IOException)
                    {
                        return false;
                    }
                    int nread = -1;

                    while (nread != 0)
                    {
                        try
                        {
                            bytetoread = connection.receive_int(sock);
                            if (bytetoread == -1)
                                return false;

                            if (bytetoread == -2)
                            {
                                return false;
                            }
                            if (bytetoread == -5)
                            {
                                break;
                            }

                            if (!connection.receive_checked(sock, ref buffer, bytetoread))
                                return false;

                            filestream.Write(buffer, 0, bytetoread);
                            filestream.Close();
                        }
                        catch (Exception)
                        {
                            return false;
                        }
                    }

                    return true;
                }
                else if (isFile == 2) //Directory
                {
                    try
                    {
                        String[] parts = fullname.Split('\\');
                        dirname = parts[parts.Length - 1];
                        Directory.CreateDirectory(fullname);
                    }
                    catch (Exception) { return false; }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception) { return false; }
        }

        private bool send_file_directory(Socket sock, String name, String fullname, bool isFile)
        {
            byte[] buffer;
            int buffer_len = 1400, res;

            //Send name
            try
            {
                buffer = Encoding.Unicode.GetBytes(name);
                res = connection.send_int(sock, buffer.Length);
                if (res == -1)
                    return false;

                sock.Send(buffer, buffer.Length, 0);

                if (isFile)
                {
                    FileStream filestream = null;
                    buffer = new byte[1400];

                    //A file is about to be sent
                    res = connection.send_int(sock, 1);
                    if (res == -1)
                        return false;
                    try
                    {
                        filestream = new FileStream(fullname, FileMode.Open, FileAccess.Read);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    int nread = -1;

                    while (nread != 0)
                    {
                        try
                        {
                            nread = filestream.Read(buffer, 0, buffer_len);
                            if (nread == 0)
                            {
                                break;
                            }
                            res = connection.send_int(sock, nread);
                            if (res == -1)
                                return false;
                            sock.Send(buffer, nread, 0);
                        }
                        catch (Exception )
                        {
                            return false;
                        }
                    }

                    res = connection.send_int(sock, -5);
                    if (res == -1)
                        return false;
                    filestream.Close();
                    //Console.WriteLine("File Sent!");
                    return true;
                }
                else //Directory
                {
                    //A directory is about to be sent
                    res = connection.send_int(sock, 2);
                    if (res == -1)
                        return false;

                    return true;
                }
            }
            catch (Exception) { return false; }
        }

    }
}
