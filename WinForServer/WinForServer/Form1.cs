﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace WinForServer
{
    public partial class FormServer : Form
    {
        public int porta;
        public String defaultPassword;
        public ScreenSize SSize;
        public AutoResetEvent block_event;
        public Thread receive_controlThread;
        public Thread receive_inputThread;
        Receive_input receive_inputObject;
        Receive_control receive_controlObject;
        Transfer form_transfer;
        Form2 redBorder;

        public FormServer()
        {
            InitializeComponent();
            this.FormClosing += this.Form1_FormClosing;
            form_transfer = new Transfer();
            form_transfer.TopMost = true;
            redBorder = new Form2();
            block_event = new AutoResetEvent(false);
            porta = 3455;
            this.defaultPassword = "toor";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            notifyIcon1.BalloonTipText = "Applicazione in esecuzione";
            notifyIcon1.BalloonTipTitle = "Server";
            notifyIcon1.Icon = new Icon(this.Icon, 40, 40);
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
            this.textBox2.Text = porta.ToString();
            this.textBox1.Text = defaultPassword;
           // this.textBox5.Text = "";log

            label2.Text = "Disconnesso";
            pictureBox1.Visible = false;
            pictureBox2.Visible = true;
            
            label4.Text = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).Select(ip => ip.ToString()).FirstOrDefault() ?? "";

            this.creaThreadControllo();
            this.creaThreadInput();
            notifyIcon1.ShowBalloonTip(1000);
        }

        public void creaThreadInput()
        {
            receive_inputObject = new Receive_input(this, redBorder);

            receive_inputThread = new Thread(receive_inputObject.DoWork);
            receive_inputThread.SetApartmentState(ApartmentState.STA);

            // Start the receive_input thread.
            receive_inputThread.Start();
        }

        public void creaThreadControllo()
        {
            receive_controlObject = new Receive_control(this, redBorder, defaultPassword);

            receive_controlThread = new Thread(receive_controlObject.DoWork);
            receive_controlThread.SetApartmentState(ApartmentState.STA);

            // Start the receive_control thread.
            receive_controlThread.Start();
        }

        public void showFormTransfer()
        {
            if (!form_transfer.IsDisposed)
                form_transfer.Show();
            else
            {
                form_transfer = new Transfer();
                form_transfer.TopMost = true;
                form_transfer.Show();
            }
        }

        public void hideFormTransfer()
        {
            if(!form_transfer.IsDisposed)
            form_transfer.Hide();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            label4.Text = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).Select(ip => ip.ToString()).FirstOrDefault() ?? "";
            this.Show();
            ShowInTaskbar = true;
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void FormServer_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            }
        }

        internal void changeLabel(string address)
        {
            string[] words = address.Split(':');
            label5.Invoke((MethodInvoker)(() =>label5.Text = words[0])); 
        }

        public void setInterfaceConnState(bool connected)
        {
            if (connected)
            {
                label2.Text = "Connesso";
                pictureBox1.Visible = true;
                pictureBox2.Visible = false;
            }
            else
            {
                label2.Text = "Disconnesso";
                pictureBox1.Visible = false;
                pictureBox2.Visible = true;
            }
        }

        private void impostazioniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            ShowInTaskbar = true;
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            receive_inputObject.CloseAll();
            if (receive_inputObject.sock_input == null)
            {
                if (receive_inputObject.sock != null)
                {
                    receive_inputObject.sock.Close();
                    receive_inputObject.sock = null;
                }
            }
            else
            {
                receive_inputObject.sock_input.Shutdown(SocketShutdown.Both);
                receive_inputObject.sock_input.Close();
                receive_inputObject.sock_input = null;
            }

            if (receive_controlObject.sock_control == null)
            {
                if (receive_controlObject.sock != null)
                {
                    receive_controlObject.sock.Close();
                    receive_controlObject.sock = null;
                }
            }
            else
            {
                receive_controlObject.sock_control.Shutdown(SocketShutdown.Both);
                receive_controlObject.sock_control.Close();
                receive_controlObject.sock_control = null;
            }
            
            receive_controlThread.Join();
            receive_inputThread.Join();
           
            Application.Exit(); 
            return;
        }

        public void restartInput()
        {
            receive_inputObject.CloseAll();
            if (receive_inputObject.sock_input == null)
            {
                if (receive_inputObject.sock != null)
                {
                    receive_inputObject.sock.Close();
                    receive_inputObject.sock = null;
                }
            }
            else
            {
                 receive_inputObject.sock_input.Shutdown(SocketShutdown.Both);
                 receive_inputObject.sock_input.Close();
                 receive_inputObject.sock_input = null;
            }

            receive_inputThread.Join();
            return;
        }

        public void restartControl()
        {
            receive_inputObject.CloseAll();
            if (receive_inputObject.sock_input == null)
            {
                if (receive_inputObject.sock != null)
                {
                    receive_inputObject.sock.Close();
                    receive_inputObject.sock = null;
                }
            }
            else
            {
                receive_inputObject.sock_input.Shutdown(SocketShutdown.Both);
                receive_inputObject.sock_input.Close();
                receive_inputObject.sock_input = null;
            }

            if (receive_controlObject.sock_control == null)
            {
                receive_controlObject.sock.Close();
                receive_controlObject.sock = null;
            }
            else
            {
                receive_controlObject.sock_control.Shutdown(SocketShutdown.Both);
                receive_controlObject.sock_control.Close();
                receive_controlObject.sock_control = null;
            }

            receive_controlThread.Join();
            return;
        }

        public void restartCambioPorta()
        {
            receive_inputObject.CloseAll();
            if (receive_inputObject.sock_input == null)
            {
                receive_inputObject.sock.Close();
                receive_inputObject.sock = null;
            }
            else
            {
                receive_inputObject.sock_input.Shutdown(SocketShutdown.Both);
                receive_inputObject.sock_input.Close();
                receive_inputObject.sock_input = null;
            }

            if (receive_controlObject.sock_control == null)
            {
                receive_controlObject.sock.Close();
                receive_controlObject.sock = null;
            }
            else
            {
                receive_controlObject.sock_control.Shutdown(SocketShutdown.Both);
                receive_controlObject.sock_control.Close();
                receive_controlObject.sock_control = null;
            }

            return;
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
            ShowInTaskbar = false;
            notifyIcon1.Visible = true;
            e.Cancel = true;        
        }


        private void button1_Click(object sender, EventArgs e)
        {
            defaultPassword = textBox6.Text;
            receive_controlObject.setPassword(defaultPassword);
            textBox1.Text = textBox6.Text;
            textBox6.Text = "";
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int door;
            DialogResult dialogResult = MessageBox.Show("Il server verrà riavviato e sarà persa l'attuale connessione, continuare comunque?", "Attenzione!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                return;
            }

            bool ok = int.TryParse(textBox2.Text, out door);
            if (!ok)
            {
                MessageBox.Show("Inserire numero di porta valido( > 1024 ) per la connessione", "Avviso",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (door < 1024)
            {
                MessageBox.Show("Inserire numero di porta valido( > 1024 ) per la connessione", "Avviso",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                this.porta = door;
                restartControl();
                receive_inputThread.Join();
                redBorder.Hide();
                setInterfaceConnState(false);
                changeLabel("---------------");
                hideFormTransfer();
                creaThreadControllo();
                creaThreadInput();
                MessageBox.Show("Porta cambiata correttamente");
            }
            
        }

        //general functions

        public int send(Stream stream, Socket sock)
        {
            int res;
            try
            {
                byte[] bufferbytes = ((MemoryStream)stream).ToArray();
                send_int(sock, bufferbytes.Length);
                res = sock.Send(bufferbytes, bufferbytes.Length, 0);
            }
            catch (Exception )
            {
                return -1;
            }

            return res;
        }


        public Stream receive(Socket connsock)
        {
            byte[] bufferbyter;
            Stream stream1;

            try
            {
                int bytetoread = receive_int(connsock);
                bufferbyter = new byte[bytetoread];
                receive_checked(connsock, ref bufferbyter, bytetoread);
                stream1 = new MemoryStream(bufferbyter);
            }
            catch (Exception)
            {
                return null;
            }

            return stream1;
        }

        public int receive_int(Socket sock)
        {
            byte[] bufn = new byte[4];
            int res;

            try
            {
                if (!receive_checked(sock, ref bufn, 4))
                {
                    return -1;
                }
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bufn);
                }

                res = BitConverter.ToInt32(bufn, 0);
            }
            catch (Exception )
            {
                return -1;
            }
            return res;
        }

        public bool receive_checked(Socket sock, ref byte[] buffer, int n)
        {
            int actual_read = 0;

            try
            {
                while (actual_read != n)
                {
                    actual_read += sock.Receive(buffer, actual_read, n - actual_read, 0);
                }
            }
            catch (Exception )
            {
                return false;
            }
            return true;
        }

        public int send_int(Socket sock, int n)
        {
            byte[] bufn = new byte[4];
            int res;
            try
            {
                bufn = BitConverter.GetBytes(n);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bufn);
                }
                res = sock.Send(bufn, 4, 0);
            }
            catch (Exception )
            {

                return -1;
            }
            return res;
        }
    }
}
