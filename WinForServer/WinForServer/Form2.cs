﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace WinForServer
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            this.TopMost = true; // make the form always on top
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None; // hidden border
            this.TransparencyKey = this.BackColor = Color.Green; // the color key to transparent, choose a color that you don't use
            this.ShowInTaskbar = false;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            int w = Screen.PrimaryScreen.Bounds.Width;
            int h = Screen.PrimaryScreen.Bounds.Height;
            this.Location = new Point(0, 0);
            this.Size = new Size(w, h);
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {
             int borderWidth = 8;
            ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle,
                                    Color.Red, borderWidth, ButtonBorderStyle.Solid,
                                    Color.Red, borderWidth, ButtonBorderStyle.Solid,
                                    Color.Red, borderWidth, ButtonBorderStyle.Solid,
                                    Color.Red, borderWidth, ButtonBorderStyle.Solid);
        }

        
    }
}