﻿using System.Runtime.InteropServices;

namespace WinForServer
{
    public static class Input
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct INPUT
        {
            [FieldOffset(0)]
            public int type;
            [FieldOffset(sizeof(int))]
            public MSLLHOOKSTRUCT mi;
            [FieldOffset(sizeof(int))]
            public KBLLHOOKSTRUCT ki;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MSLLHOOKSTRUCT
        {
            public int dx;
            public int dy;
            public int mouseData;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct KBLLHOOKSTRUCT
        {
            public short vkCode;
            public short scanCode;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }


        [DllImport("User32.dll", SetLastError = true)]
        public static extern int SendInput(int nInputs, ref INPUT pInputs, int cbSize);
    }
}
