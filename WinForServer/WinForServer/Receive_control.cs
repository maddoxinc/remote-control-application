﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WinForServer
{
    class Receive_control
    {
        FormServer form;
        public Socket sock;
        public Socket sock_control;
        byte[] salt;
        byte[] hash_correctPassword;
        byte[] b_correctPassword;
        Form2 redBorder;
        ScreenSize SSize;
        String correctPassword;
      

        public Receive_control(FormServer formServer, Form2 redBorder, String password)
        {
            this.form = formServer;
            this.redBorder = redBorder;
            this.correctPassword = password;
        }

        public void setPassword(String password)
        {
            correctPassword = password;
        }

        public void DoWork()
        {
        start:
            XmlSerializer xs;
                    
            bool condition = true;
            //Gestione login
            
            Stream stream4;
            while (condition)
            {
                try
                {
                    try
                    {
                        xs = new XmlSerializer(typeof(String));
                    }
                    catch (InvalidOperationException) { throw new Exception(); }

                    
                    //////////////////////Gestione login

                    sock_control = accept("control", "", form.porta);
                    if (sock_control == null)
                    {
                        throw new SocketException();
                    }

                aspetta_password:

                    int res = form.receive_int(sock_control);
                    if (res < 0)
                        throw new Exception();
                    if (res == 1)
                    {
                        //Login annullato dall'utente
                        throw new Exception();
                    }
                    try
                    {
                        xs = new XmlSerializer(typeof(String));
                    }
                    catch (InvalidOperationException ) { throw new Exception(); }

                    try
                    {
                        b_correctPassword = Encoding.UTF8.GetBytes(correctPassword);
                    }
                    catch (ArgumentNullException ) { throw new Exception(); }
                    catch (EncoderFallbackException ) { throw new Exception(); }
                   
                    //hash
                    salt = new byte[2] { 1, 1 };
                    hash_correctPassword = GenerateSaltedHash(b_correctPassword, salt);

                    int bytetoread = form.receive_int(sock_control);
                    if (bytetoread < 0)
                        throw new Exception();

                    byte[] b_password = new byte[bytetoread];

                    if (!form.receive_checked(sock_control, ref b_password, bytetoread))
                        throw new Exception();

                    string risp;

                    if (CompareByteArrays(b_password, hash_correctPassword))//password corretta
                        risp = "ok";
                    else
                        risp = "no";

                    Stream stream2 = new MemoryStream();
                    try
                    {
                        xs.Serialize(stream2, risp);
                    }
                    catch (InvalidOperationException ) { throw new Exception(); }
                    
                     res = form.send(stream2, sock_control);
                    if(res < 0)
                        throw new Exception();


                    if (risp.CompareTo("ok") == 0)
                    {
                        
                        Stream stream3 = form.receive(sock_control);                    
                        if(stream3 == null)
                            throw new SocketException();

                        SSize = new ScreenSize();
                        try
                        {
                            xs = new XmlSerializer(typeof(ScreenSize));
                        }
                        catch (InvalidOperationException ) { throw new Exception(); }

                        try
                        {
                            SSize = (ScreenSize)xs.Deserialize(stream3);
                        }
                        catch (InvalidOperationException ) { throw new Exception(); }
                        form.SSize = SSize;
                        condition = false;//<----------------------
                        
                    }
                    else
                        goto aspetta_password;
                }
                catch (SocketException )//chiusura controllata
                {

                    Console.WriteLine("chiusura receive_control");
                    return;
                }
                catch (InvalidOperationException )//chiusura controllata
                {

                  Console.WriteLine("chiusura receive_control");
                    return;                    
                }
                catch (Exception )
                {
                    return;
                }
            }
      
            try
            {
                while (true)
                {
                    String target,notarget;
                    //receive che aspetta di sapere quando è il target
                    stream4 = form.receive(sock_control);
                    if (stream4 == null)
                        throw new SocketException();

                    try
                    {
                        xs = new XmlSerializer(typeof(String));
                    }
                    catch (InvalidOperationException ) { throw new Exception(); }

                    try
                    { 
                       target = (String)xs.Deserialize(stream4);
                    }
                    catch (InvalidOperationException ) { throw new InvalidOperationException(); }

                    if (target.CompareTo("target") == 0)
                    {
                        //show red border
                        form.Invoke((MethodInvoker)delegate()
                        {
                            redBorder.Show();
                            form.setInterfaceConnState(true);
                            form.changeLabel(sock_control.RemoteEndPoint.ToString());
                        });
                    }
                    else if (target.CompareTo("disconnect") == 0)//richiesta da parte del client
                    {
                        form.Invoke((MethodInvoker)delegate()
                        {
                            redBorder.Hide();
                            form.setInterfaceConnState(false);
                            form.changeLabel("---------------");
                            form.hideFormTransfer();
                        });

                        form.restartInput();
                        form.creaThreadInput();
                        if (sock_control != null)
                        {
                            sock_control.Shutdown(SocketShutdown.Both);
                            sock_control.Close();
                            sock_control = null;
                        }
                        goto start;
                    }

                    try
                    {
                        xs = new XmlSerializer(typeof(String));
                       
                    }
                    catch (InvalidOperationException ) { throw new Exception(); }

                    while (true)
                    {

                        stream4 = form.receive(sock_control);
                        if (stream4 == null)
                            throw new SocketException();

                        try 
                        { 
                          notarget = (String)xs.Deserialize(stream4);
                        }
                        catch (InvalidOperationException ) { throw new InvalidOperationException(); }

                        if (notarget.CompareTo("notarget") == 0)
                        {
                            //hide red border
                            form.Invoke((MethodInvoker)delegate()
                            {
                                redBorder.Hide();
                               // form.hideFormTransfer();
                            });
                            break;
                        }
                        else if (notarget.CompareTo("send clipboard") == 0)
                        {
                            form.block_event.Set();
                        }
                        else if (notarget.CompareTo("disconnect") == 0)//richiesta da parte del client
                        {
                            form.Invoke((MethodInvoker)delegate()
                            {
                                redBorder.Hide();
                                form.setInterfaceConnState(false);
                                form.changeLabel("---------------");
                                form.hideFormTransfer();
                            });

                            form.restartInput();
                            form.creaThreadInput();
                            if(sock_control != null)
                            {
                                sock_control.Shutdown(SocketShutdown.Both);
                                sock_control.Close();
                                sock_control = null;
                            }
                            goto start;                   
                        }
                        else
                        {
                            Console.WriteLine("comando sconosciuto");
                        }
                    }
                }
            }
            catch (SocketException )//chiusura controllata
            {
              
                Console.WriteLine("chiusura receive_control");
                return;
            }
            catch (InvalidOperationException )//chiusura controllata
            {
                
                Console.WriteLine("chiusura receive_control");
                return;
            }
            catch (Exception ) //torna il listening
            {
                form.restartInput();
                form.creaThreadInput();
                goto start;
            }

        }

        static byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            HashAlgorithm algorithm;
            byte[] plainTextWithSaltBytes;
            byte[] res;

            try
            {
                algorithm = new SHA256Managed();
                plainTextWithSaltBytes = new byte[plainText.Length + salt.Length];

                for (int i = 0; i < plainText.Length; i++)
                {
                    plainTextWithSaltBytes[i] = plainText[i];
                }
                for (int i = 0; i < salt.Length; i++)
                {
                    plainTextWithSaltBytes[plainText.Length + i] = salt[i];
                }
                 
                res = algorithm.ComputeHash(plainTextWithSaltBytes);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return res;
        }

        public static bool CompareByteArrays(byte[] array1, byte[] array2)
        {
            if (array1.Length != array2.Length)
            {
                return false;
            }

            for (int i = 0; i < array1.Length; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }

            return true;
        }


        public Socket accept(String conn_name, String address, int port)
        {
            SocketPermission sockperm;
            IPEndPoint iep;

            //Console.WriteLine("Initializing...\n");

            sockperm = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, address, SocketPermission.AllPorts);
            sockperm.Demand();
            //Console.WriteLine("Permission granted!\n");

            IPHostEntry iph = Dns.GetHostEntry(address);
            IPAddress ipa = iph.AddressList[0];
            iep = new IPEndPoint(IPAddress.Any, port);
            try
            {
                sock = new Socket(iep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //Console.WriteLine("Socket " + conn_name + " created!\n");

            try
            {
                sock.Bind(iep);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //Console.WriteLine("Socket bound!\n");


            try
            {
                sock.Listen(1); //Quanti ne devono stare in coda??
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Listening...\n");

            try
            {
                sock_control = sock.Accept();
                sock.Close();
                sock = null;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Client " + conn_name + " connected!\n");
           
            return sock_control;
        }
    }
}
