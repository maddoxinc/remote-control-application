﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace WinForServer
{
    public class Receive_input
    {
        public Socket sock;
        public Socket sock_input;
        public Worker workerObject;
        public Send_clipboard worker_sendClipboard;
        private FormServer form;
        Form2 redBorder;
        Thread workerThread;
        Thread thread_sendClipboard;

        public Receive_input(FormServer formServer, Form2 redBorder)
        {
            this.form = formServer;
            this.redBorder = redBorder;
        }

        public void DoWork()
        {
            //input
            start:
            XmlSerializer xs = null;
            bool exit_condition = true;

            
            workerObject = new Worker(form, redBorder);
            workerThread = new Thread(workerObject.DoWork);
            workerThread.SetApartmentState(ApartmentState.STA);

            // Start the worker thread.
            workerThread.Start();

            worker_sendClipboard = new Send_clipboard(form, form.block_event);
            thread_sendClipboard = new Thread(worker_sendClipboard.DoWork);
            thread_sendClipboard.SetApartmentState(ApartmentState.STA);

            // Start the sendClipboard thread.
            thread_sendClipboard.Start();

            while (exit_condition)
            {
                try
                {
                    Input.INPUT inputmsg = new Input.INPUT();

                    try
                    {
                        xs = new XmlSerializer(inputmsg.GetType());
                    }
                    catch (InvalidOperationException) 
                    {
                        Console.WriteLine("Serializer");
                        throw new Exception(); 
                    }

                    sock_input = accept("input", "", form.porta+1 );
                    if (sock_input == null)
                    {
                        throw new SocketException();
                    }

                    Stream stream1 = null;
                    while (true)
                    {
                        stream1 = form.receive(sock_input);
                        if (stream1 == null)
                            throw new SocketException();

                        try
                        {
                            inputmsg = (Input.INPUT)xs.Deserialize(stream1);
                        }
                        catch (InvalidOperationException) 
                        {
                           throw new InvalidOperationException(); 
                        }

                        if (inputmsg.type == 0)
                        {
                            //Proportion between resolution of server and client screen
                            inputmsg.mi.dx = inputmsg.mi.dx * Screen.PrimaryScreen.Bounds.Width / form.SSize.width;
                            inputmsg.mi.dy = inputmsg.mi.dy * Screen.PrimaryScreen.Bounds.Height / form.SSize.height;
                            //Proportion between pixel coordinates and hardware coordinates
                            inputmsg.mi.dx = inputmsg.mi.dx * 65335 / Screen.PrimaryScreen.Bounds.Width;
                            inputmsg.mi.dy = inputmsg.mi.dy * 65335 / Screen.PrimaryScreen.Bounds.Height;
                        }


                        if (Input.SendInput(1, ref inputmsg, Marshal.SizeOf(inputmsg)) == 0)
                        {
                            Console.WriteLine("errore sendInput");
                        }






                        
                    }
                }
                catch (SocketException)//chiusura controllata
                {
                    exit_condition = false;
                    worker_sendClipboard.block_event.Set();
                    workerThread.Join();
                    thread_sendClipboard.Join();
                    Console.WriteLine("chiusura receive_input");
                }
                catch (InvalidOperationException )//chiusura controllata
                {
                    exit_condition = false;
                    worker_sendClipboard.block_event.Set();
                    workerThread.Join();
                    thread_sendClipboard.Join();
                    Console.WriteLine("chiusura receive_input");
                }
                catch (Exception )//eccezione generica, torna in listening
                {
                    worker_sendClipboard.block_event.Set();
                    form.restartControl();
                    workerThread.Join();
                    thread_sendClipboard.Join();
                    form.creaThreadControllo();                    
                    goto start;
                }
            }

        return;
        }

        

        public void CloseAll() 
        {
            if (workerThread != null)
            {
                if (workerObject.sock != null)
                {
                    workerObject.sock.Close();
                    workerObject.sock = null;
                }
                if (workerObject.connsock != null)
                {
                    workerObject.connsock.Shutdown(SocketShutdown.Both);
                    workerObject.connsock.Close();
                    workerObject.connsock = null;
                }
            }

            if (thread_sendClipboard!= null)
            {
                if (worker_sendClipboard.sock != null)
                {
                    worker_sendClipboard.sock.Close();
                    worker_sendClipboard.sock = null;
                }
                if (worker_sendClipboard.sock_clip != null)
                {
                    worker_sendClipboard.sock_clip.Shutdown(SocketShutdown.Both);
                    worker_sendClipboard.sock_clip.Close();
                    worker_sendClipboard.sock_clip = null;
                }
            }  
        }

        public Socket accept(String conn_name, String address, int port)
        {
            SocketPermission sockperm;
            IPEndPoint iep;

           // Console.WriteLine("Initializing...\n");

            sockperm = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, address, SocketPermission.AllPorts);
            sockperm.Demand();
          //  Console.WriteLine("Permission granted!\n");

            IPHostEntry iph = Dns.GetHostEntry(address);
            IPAddress ipa = iph.AddressList[0];
            iep = new IPEndPoint(IPAddress.Any, port);
            try
            {
                sock = new Socket(iep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
          //  Console.WriteLine("Socket " + conn_name + " created!\n");

            try
            {
                sock.Bind(iep);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
          //  Console.WriteLine("Socket bound!\n");


            try
            {
                sock.Listen(1); //Quanti ne devono stare in coda??
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Listening...\n");

            try
            {
                sock_input = sock.Accept();
                sock.Close();
                sock = null;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Client " + conn_name + " connected!\n"); 

            return sock_input;
        }
    }
}
