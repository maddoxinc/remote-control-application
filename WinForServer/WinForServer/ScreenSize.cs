﻿using System;

namespace WinForServer
{
    [Serializable]
    public class ScreenSize
    {
        public int width, height;

        public ScreenSize() { }

        public ScreenSize(int w, int h)
        {
            this.width = w;
            this.height = h;
        }
    }
}
