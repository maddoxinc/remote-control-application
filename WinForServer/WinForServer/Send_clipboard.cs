﻿using System;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WinForServer
{
    public class Send_clipboard
    {
        public Socket sock;
        public Socket sock_clip;
        public Mutex m;
        public FormServer form;
        public AutoResetEvent block_event;

        public Send_clipboard(FormServer form, AutoResetEvent ev)
        {
            this.block_event = ev;
            this.form = form;
            this.block_event.Reset();
        }

        public void DoWork()
        {
            int res;
            bool flag = true;
            try
            {
                sock_clip = accept("send_clipboard", "", form.porta + 3);
                if (sock_clip == null)
                {
                    throw new SocketException();
                }
            }
            catch (SocketException)
            {
               Console.WriteLine("chiusura send_clipboard");
                flag = false;
            }

            while (flag)
            {
                try
                {
                    block_event.WaitOne();

                    if (sock_clip == null)
                        throw new SocketException();

                    XmlSerializer clipSer = null;
                    string myStr = null;
                    DataObject retrievedData = (DataObject)Clipboard.GetDataObject();

                    if (retrievedData.GetDataPresent(DataFormats.Rtf))
                    {

                        if (!send_format(DataFormats.Rtf))
                            throw new SocketException();

                        myStr = (String)Clipboard.GetData(DataFormats.Rtf);
                        clipSer = new XmlSerializer(myStr.GetType());
                        Stream stream = new MemoryStream();
                        clipSer.Serialize(stream, myStr);
                        res = form.send(stream, sock_clip);
                        if (res == -1)
                            throw new SocketException();

                        Console.WriteLine("Contenuto della clipboard inviato!");                    
                    }
                    else if (retrievedData.GetDataPresent(DataFormats.Text))
                    {
                        if (!send_format(DataFormats.Text))
                            throw new SocketException();

                        myStr = Clipboard.GetText();

                        clipSer = new XmlSerializer(myStr.GetType());

                        Stream stream = new MemoryStream();
                        clipSer.Serialize(stream, myStr);
                        res = form.send(stream, sock_clip);
                        if (res == -1)
                            throw new SocketException();

                        Console.WriteLine("Contents of the clipboard sent!\n");
                    }
                    else if (retrievedData.GetDataPresent(DataFormats.Bitmap))
                    {
                        if (!send_format(DataFormats.Bitmap))
                            throw new SocketException();

                        Bitmap myBmp = (Bitmap)Clipboard.GetData(DataFormats.Bitmap);
                        ImageConverter converter = new ImageConverter();

                        byte[] bufferbytes = (byte[])converter.ConvertTo(myBmp, typeof(byte[]));

                        byte[] bufferl = BitConverter.GetBytes(bufferbytes.Length);
                        if (BitConverter.IsLittleEndian)
                        {
                            Array.Reverse(bufferl);
                        }

                        sock_clip.Send(bufferl, bufferl.Length, 0);
                        sock_clip.Send(bufferbytes, bufferbytes.Length, 0);

                      Console.WriteLine("Contents of the clipboard sent!\n");

                    }
                    else if (retrievedData.GetDataPresent(DataFormats.FileDrop))
                    {
                        //invio file

                        Console.WriteLine("filedrop!");

                        if (!send_format(DataFormats.FileDrop))
                            throw new SocketException();

                        res = form.receive_int(sock_clip);
                        if (res == -1)
                            throw new SocketException();

                        if (res == 3) {
                            form.Invoke((MethodInvoker)form.showFormTransfer);
                            StringCollection sc = Clipboard.GetFileDropList();
                            int n = sc.Count;

                            //Send number of elements on top level
                            res = form.send_int(sock_clip, n);
                            if (res == -1)
                                throw new SocketException();

                            for (int i = 0; i < n; i++)
                            {
                                FileAttributes attr = File.GetAttributes(sc[i]);

                                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                                {
                                    //Directory
                                    DirectoryInfo dir = new DirectoryInfo(sc[i]);
                                    DirectoryInfo[] subdirs = dir.GetDirectories();
                                    FileInfo[] subfiles = dir.GetFiles();
                                    if (!send_file_directory(sock_clip, dir.Name, dir.FullName, false))
                                        throw new Exception();

                                    //Send number of elements on sub level
                                    res = form.send_int(sock_clip, subdirs.Length + subfiles.Length);
                                    if (res == -1)
                                        throw new SocketException();

                                    if (!detect_structure(subdirs, subfiles))
                                    {
                                        throw new SocketException();
                                    }
                                }
                                else
                                {
                                    //File
                                    FileInfo fil = new FileInfo(sc[i]);
                                    if (!send_file_directory(sock_clip, fil.Name, fil.FullName, true))
                                        throw new SocketException();
                                }
                            }
                            form.Invoke((MethodInvoker)form.hideFormTransfer);
                        }
                        else
                        {
                            Console.WriteLine("trasferimento annullato dall'utente");
                        }
                    }
                    else
                    {
                        Console.WriteLine("altro formato!!!!");
                    }
                    
                }
                catch(SocketException )
                {
                    Console.WriteLine("chiusura send_clipboard");
                    flag = false;
                }
                catch (InvalidOperationException )
                {
                    Console.WriteLine("chiusura send_clipboard");
                    flag = false;
                }
                catch (Exception )
                {
                    Console.WriteLine("chiusura imprevista send_clipboard");
                    goto stop;
                }
            }
        stop:

            return;
        }

        private bool send_format(string format)
        {
            XmlSerializer clipSer = null;
            int res;

            try
            {
                clipSer = new XmlSerializer(format.GetType());
                Stream stream = new MemoryStream();
                clipSer.Serialize(stream, format);
                res = form.send(stream, sock_clip);
                if (res == -1)
                    return false;
            }
            catch (Exception )
            {
                return false;
            }

            return true;
        }

        private bool detect_structure(DirectoryInfo[] dirs, FileInfo[] files)
        {
            int nd,nf,res;

            try
            {
                nd = dirs.Length;
                nf = files.Length;;
                for (int i = 0; i < nd; i++)
                {
                    DirectoryInfo[] subdirs = dirs[i].GetDirectories();
                    FileInfo[] subfiles = dirs[i].GetFiles();
                    if (!send_file_directory(sock_clip, dirs[i].Name, dirs[i].FullName, false))
                        return false;

                    //Send number of elements on sub level
                    res = form.send_int(sock_clip, subdirs.Length + subfiles.Length);
                    if (res == -1)
                        return false;

                    if (!detect_structure(subdirs, subfiles))
                        return false;
                }

                for (int i = 0; i < nf; i++)
                {
                    if (!send_file_directory(sock_clip, files[i].Name, files[i].FullName, true))
                        return false;
                }
            }
            catch (Exception )
            {
                return false;
            }

            return true;
        }

        private  bool receive_file_directory(Socket sock, String path, ref String dirname)
        {
            byte[] buffer = new byte[1400];
            int bytetoread;
            String fullname;

            try
            {
                //Receive name
                bytetoread = form.receive_int(sock);
                if (bytetoread ==  -1)
                    return false;

                if(!form.receive_checked(sock, ref buffer, bytetoread))
                    return false;

                fullname = path + "" + Encoding.Unicode.GetString(buffer, 0, bytetoread);

                //Receive code (file or directory)
                int isFile = form.receive_int(sock);

                if (isFile == 1)
                {
                    dirname = null;
                    
                    FileStream filestream = null;

                    if (File.Exists(fullname))
                    {
                        File.WriteAllText(fullname, string.Empty);
                    }

                    try
                    {
                        filestream = new FileStream(fullname, FileMode.OpenOrCreate, FileAccess.Write);
                    }
                    catch (System.IO.IOException )
                    {
                        return false;
                    }
                    int nread = -1;

                    while (nread != 0)
                    {
                        try
                        {
                            bytetoread = form.receive_int(sock);
                            if (bytetoread == -1)
                                return false;

                            if (bytetoread == -2)
                            {
                                return false;
                            }
                            if (bytetoread == -5)
                            {
                                break;
                            }

                            if (form.receive_checked(sock, ref buffer, bytetoread))
                                return false;

                            filestream.Write(buffer, 0, bytetoread);
                        }
                        catch (Exception )
                        {
                            return false;
                        }
                    }

                    filestream.Close();
                   Console.WriteLine("File received!");
                    return true;
                }
                else if (isFile == 2) //Directory
                {
                    String[] parts = fullname.Split('\\');
                    dirname = parts[parts.Length - 1];
                    Directory.CreateDirectory(fullname);
                    return true;
                }
                else
                {
                    return false;
                }
            }                      
            catch (Exception )
            {
                return false;
            }
        }

        private  bool send_file_directory(Socket sock, String name, String fullname, bool isFile)
        {
            byte[] buffer;
            int buffer_len = 1400,res;

            try
            {
                //Send name
                buffer = Encoding.Unicode.GetBytes(name);
                res = form.send_int(sock, buffer.Length);
                if (res == -1)
                    return false;
                sock.Send(buffer, buffer.Length, 0);

                if (isFile)
                {
                    FileStream filestream = null;
                    buffer = new byte[1400];
                   Console.WriteLine("Sending file " + fullname);

                    //A file is about to be sent
                    res = form.send_int(sock, 1);
                    if (res == -1)
                        return false;

                    try
                    {
                        filestream = new FileStream(fullname, FileMode.Open, FileAccess.Read);
                    }
                    catch (System.IO.IOException e)
                    {
                      Console.WriteLine(e.Message);
                        return false;
                    }
                    int nread = -1;

                    while (nread != 0)
                    {
                        try
                        {
                            nread = filestream.Read(buffer, 0, buffer_len);
                            if (nread == 0)
                            {
                                break;
                            }
                            res = form.send_int(sock, nread);
                            if (res == -1)
                                return false;
                            sock.Send(buffer, nread, 0);
                        }
                        catch (Exception e)
                        {
                           Console.WriteLine(e.Message);
                            return false;
                        }

                    }

                    res = form.send_int(sock, -5);
                    if (res == -1)
                        return false;

                    filestream.Close();
                    return true;
                }
                else //Directory
                {
                    //A directory is about to be sent
                    res = form.send_int(sock, 2);
                    if (res == -1)
                        return false;

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }

        public Socket accept(String conn_name, String address, int port)
        {
            SocketPermission sockperm;
            IPEndPoint iep;



            sockperm = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, address, SocketPermission.AllPorts);
            sockperm.Demand();


            IPHostEntry iph = Dns.GetHostEntry(address);
            IPAddress ipa = iph.AddressList[0];
            iep = new IPEndPoint(IPAddress.Any, port);
            try
            {
                sock = new Socket(iep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

            try
            {
                sock.Bind(iep);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
           

            try
            {
                sock.Listen(1); //Quanti ne devono stare in coda??
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Listening...\n");

            try
            {
                sock_clip = sock.Accept();
                sock.Close();
                sock = null;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Client " + conn_name + " connected!\n");
           
            return sock_clip;
        }
    }
}
