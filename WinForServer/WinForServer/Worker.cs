﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace WinForServer
{
    public class Worker
    {
        public Socket connsock;
        public Socket sock;
        private FormServer form;
        Form2 redBorder;

        public Worker(FormServer formServer, Form2 redBorder)
        {
            this.form = formServer;
            this.redBorder = redBorder;
        }

        public Worker()
        {
           
        }

        public void DoWork()
        {
            byte[] bufferbyter = new byte[1];
            byte[] bufferbyterr = new byte[1024];
            byte[] bufferl = new byte[4];
            bool flag = true;
            int res;

            while (flag)
            {
                try
                {
                    connsock = accept("clipboard", "", form.porta + 2);
                    if (connsock == null)
                    {
                        throw new SocketException();
                    }

                    while (true)
                    {
                        Stream stream1 = form.receive(connsock);
                        if(stream1 == null)
                            throw new SocketException();
                        String format = receive_format(stream1);
                        if(format == null)
                            throw new SocketException();

                        if (format == DataFormats.Bitmap)
                        {
                            XmlSerializer clipSer = new XmlSerializer(typeof(Bitmap));
                            stream1 = form.receive(connsock);
                            Bitmap bmp = new Bitmap(stream1);
                          Console.WriteLine("clipboard data receveid: Bitmap");
                            Clipboard.SetDataObject(bmp, true);
                        }
                        else if (format == DataFormats.Rtf)
                        {
                            XmlSerializer clipSer = new XmlSerializer(typeof(String));

                            stream1 = form.receive(connsock);
                            string data = (string)clipSer.Deserialize(stream1);
                           Console.WriteLine("clipboard data receveid rtf" + data);
                            Clipboard.SetData(DataFormats.Rtf, data.Replace("\n","\r\n"));
                        }
                        else if (format == DataFormats.Text)
                        {
                            XmlSerializer clipSer = new XmlSerializer(typeof(String));
                            stream1 = form.receive(connsock);
                            string data = (string)clipSer.Deserialize(stream1);
                         Console.WriteLine("clipboard data receveid:" + data);
                            Clipboard.SetData(DataFormats.Text, data.Replace("\n", "\r\n"));
                        }
                        else if (format == DataFormats.FileDrop)
                        {
                            Console.WriteLine("Filedroppppp!");
                            String sel_path;
                            int elements;

                            FolderBrowserDialog dialog1 = new FolderBrowserDialog();
                            dialog1.Description = "Choose destination folder";

                            if (dialog1.ShowDialog() == DialogResult.OK)
                            { 
                                //fai iniziare il trasferimento al client
                                res = form.send_int(connsock, 3);
                                if (res == -1)
                                    throw new Exception();

                                form.Invoke((MethodInvoker)form.showFormTransfer);
                                sel_path = dialog1.SelectedPath;

                                //Add trailing "//" if necessary
                                if (sel_path[sel_path.Length - 1] != '\\')
                                {
                                    sel_path = sel_path + "\\";
                                }

                                elements = form.receive_int(connsock);

                                if (create_structure(sel_path, elements))
                                {
                                   Console.WriteLine("Copia avvenuta con successo!");
                                }
                                else
                                {
                                   Console.WriteLine("Errore durante la copia!");
                                }
                                form.Invoke((MethodInvoker)form.hideFormTransfer);
                            }
                            else
                            {
                                //blocca il trasferimento
                                res = form.send_int(connsock, 2);
                                if (res == -1)
                                    throw new Exception();
                                Console.WriteLine("Operazione cancellata o fallita");
                            }
                        }
                        else
                        {
                          Console.WriteLine("Formato sconosciuto!");
                        }
                    }
                }
                catch(SocketException ode)//chiusura controllata
                {
                    Console.WriteLine(ode.Message);
                    Console.WriteLine("chiusura worker");
                    goto stop;
                }
                catch (InvalidOperationException ioe)//chiusura controllata
                {
                    Console.WriteLine(ioe.Message);
                    Console.WriteLine("chiusura worker");
                    goto stop;
                }
                catch (Exception e2)
                {
                    Console.WriteLine(e2.Message);
                    Console.WriteLine("chiusura imprevista worker!");
                    goto stop;
                }

            }

        stop:

            return;
        }

        private string receive_format(Stream stream)
        {
            XmlSerializer xs;
            String res;

            try
            {
                xs = new XmlSerializer(typeof(String));
                res = (String)xs.Deserialize(stream);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }

            return res;
        }

        public bool create_structure(String path, int elements)
        {
            int subelements;

            try
            {
                for (int i = 0; i < elements; i++)
                {
                    String dirname = null;
                    if (receive_file_directory(connsock, path, ref dirname) == -1)
                    {
                        return false;
                    }

                    if (dirname == null) //File
                    {

                    }
                    else //Directory
                    {
                        subelements = form.receive_int(connsock);
                        if (subelements < 0)
                            return false;

                        if (!create_structure(path + dirname + "\\", subelements))
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new SocketException();
            }

            return true;
        }

        private int receive_file_directory(Socket sock, String path, ref String dirname)
        {
            byte[] buffer = new byte[1400];
            int bytetoread;
            String fullname;

            try
            {
                //Receive name
                bytetoread = form.receive_int(sock);
                if (bytetoread < 0)
                    return -1;

                if (!form.receive_checked(sock, ref buffer, bytetoread))
                    return -1;

                fullname = path + "" + Encoding.Unicode.GetString(buffer, 0, bytetoread);

                //Receive code (file or directory)
                int isFile = form.receive_int(sock);
                if (isFile < 0)
                    return -1;

                if (isFile == 1)
                {
                    dirname = null;
                    Console.WriteLine("Receiving file " + fullname);
                    FileStream filestream = null;

                    if (File.Exists(fullname))
                    {
                        File.WriteAllText(fullname, string.Empty);
                    }

                    filestream = new FileStream(fullname, FileMode.OpenOrCreate, FileAccess.Write);
                    int nread = -1;

                    while (nread != 0)
                    {

                        bytetoread = form.receive_int(sock);
                        if (bytetoread == -1)
                            return -1;

                        if (bytetoread == -2)
                        {
                          Console.WriteLine("Error in client (wrong file?)");
                            return 0;
                        }
                        if (bytetoread == -5)
                        {
                            break;
                        }

                        if(!form.receive_checked(sock, ref buffer, bytetoread))
                            return -1;

                        filestream.Write(buffer, 0, bytetoread);
                    }

                    filestream.Close();
                    Console.WriteLine("File received!");
                    return 1;
                }
                else if (isFile == 2) //Directory
                {
                    String[] parts = fullname.Split('\\');
                    dirname = parts[parts.Length - 1];
                    Console.WriteLine("Receiving directory " + fullname);
                    Directory.CreateDirectory(fullname);
                    return 1;
                }
                else
                {
                    Console.WriteLine("Errore codice(isFile)!");
                    return -1;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new SocketException();
            }
        }

        private bool send_file_directory(Socket sock, String name, String fullname, bool isFile)
        {
            byte[] buffer;
            int buffer_len = 1400;
            int res;

            try
            {
                //Send name
                buffer = Encoding.Unicode.GetBytes(name);
                res = form.send_int(sock, buffer.Length);
                if (res == -1)
                    return false;

                sock.Send(buffer, buffer.Length, 0);

                if (isFile)
                {
                    FileStream filestream = null;
                    buffer = new byte[1400];
                    Console.WriteLine("Sending file " + fullname);

                    //A file is about to be sent
                    res = form.send_int(sock, 1);
                    if(res == -1)
                        return false;

                    filestream = new FileStream(fullname, FileMode.Open, FileAccess.Read);

                    int nread = -1;

                    while (nread != 0)
                    {
                        nread = filestream.Read(buffer, 0, buffer_len);
                        if (nread == 0)
                        {
                            break;
                        }
                        res = form.send_int(sock, nread);
                        if(res == -1)
                            return false;
                        sock.Send(buffer, nread, 0);
                    }

                    res = form.send_int(sock, -5);
                    if (res == -1)
                        return false;

                    filestream.Close();
                    Console.WriteLine("File Sent!");
                    return true;
                }
                else //Directory
                {
                    //A directory is about to be sent
                    Console.WriteLine("Communicating directory " + fullname);
                    res = form.send_int(sock, 2);
                    if (res == -1)
                        return false;

                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public Socket accept(String conn_name, String address, int port)
        {
            SocketPermission sockperm;
            IPEndPoint iep;
            
            //Console.WriteLine("Initializing...\n");

            sockperm = new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, address, SocketPermission.AllPorts);
            sockperm.Demand();
            //Console.WriteLine("Permission granted!\n");

            IPHostEntry iph = Dns.GetHostEntry(address);
            IPAddress ipa = iph.AddressList[0];
            iep = new IPEndPoint(IPAddress.Any, port);
            try
            {
                sock = new Socket(iep.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //Console.WriteLine("Socket " + conn_name + " created!\n");

            try
            {
                sock.Bind(iep);
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            //Console.WriteLine("Socket bound!\n");


            try
            {
                sock.Listen(1); //Quanti ne devono stare in coda??
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Listening...\n");
            
            try
            {
                connsock = sock.Accept();
                sock.Close();
                sock = null;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("Client " + conn_name + " connected!\n");
          
            return connsock;
        }
    }
}
